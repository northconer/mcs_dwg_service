﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{
    #region INS DocumentByUploadImg
    [DataContract]
    public class INS_DocumentByUploadImg
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public string user_update { get; set; }
    }
    #endregion

    #region INS Repair
    [DataContract]
    public class INS_GetRepair
    {
        [DataMember]
        public string as_id { get; set; }

        [DataMember]
        public int priority { get; set; }

        [DataMember]
        public string user_rp { get; set; }

        [DataMember]
        public string request_date { get; set; }

        [DataMember]
        public string rp_detail { get; set; }

        [DataMember]
        public string admin_update { get; set; }
    }
    #endregion

    #region INS RequestByRepair
    [DataContract]
    public class INS_GetRequestByRepair
    {
        [DataMember]
        public string rp_id { get; set; }

        [DataMember]
        public string cp_item { get; set; }

        [DataMember]
        public int issue { get; set; }

        [DataMember]
        public string user_use { get; set; }

        [DataMember]
        public string user_create { get; set; }

        [DataMember]
        public string rq_detail { get; set; }

        [DataMember]
        public string user_update { get; set; }
    }
    #endregion
}