﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iShowSchedule
    {
        [DataMember]
        public string Process { get; set; }

        [DataMember]
        public string Shape_Plan { get; set; }

        [DataMember]
        public string Shape_Act { get; set; }

        //[DataMember]
        //public string TestDB { get; set; }

        //[DataMember]
        //public string Shape_2_Act { get; set; }

      

        //internal void Add(iShowSchedule iShowSchedule)
        //{
        //    throw new NotImplementedException();
        //}
    }

    [DataContract]
    public class iShowDatabase
    {
        [DataMember]
        public int ProjectNumber { get; set; }

        [DataMember]
        public string ProjectName { get; set; }

        [DataMember]
        public string ProjectDetail { get; set; }

        [DataMember]
        public string ProjectShapeMin { get; set; }

        [DataMember]
        public string ProjectShapeMax { get; set; }

    }

    [DataContract]
    public class iShowAllData
    {
        [DataMember]
        public int Project_Number { get; set; }

        [DataMember]
        public string Project_Name { get; set; }

        [DataMember]
        public string Project_Detail { get; set; }

        [DataMember]
        public string Project_ShapeMin { get; set; }

        [DataMember]
        public string Project_ShapeMax { get; set; }

    }

    [DataContract]
    public class iSetSchedule
    {
        [DataMember]
        public int Project_Number { get; set; }

        [DataMember]
        public string Process_Name { get; set; }

        [DataMember]
        public string Process_From { get; set; }

        [DataMember]
        public string Process_To { get; set; }

        [DataMember]
        public string Process_Order { get; set; }

    }

    [DataContract]
    public class iShowReport
    {
        [DataMember]
        public int Project_Number { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Shape { get; set; }

        [DataMember]
        public string Dwg_NO { get; set; }

        [DataMember]
        public string Dwg_Code { get; set; }

        [DataMember]
        public string Precess { get; set; }

        [DataMember]
        public string From { get; set; }

        [DataMember]
        public string To { get; set; }

        [DataMember]
        public string Plan { get; set; }

        [DataMember]
        public string Actual { get; set; }

    }


}