﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iLoginMcsFg
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }
    }

    [DataContract]
    public class mLoginMcsFg
    {
        [DataMember]
        public string US_ID { get; set; }
    }

    [DataContract]
    public class iUserCheckMenu
    {
        [DataMember]
        public string UserID { get; set; }
    }

    public class mUserCheckMenu
    {
        [DataMember]
        public string MN_ID { get; set; }
    }

    [DataContract]
    public class mCheckAuthen 
    {
        [DataMember]
        public string UserID { get; set; }

        [DataMember]
        public int MN_ID { get; set; } 
    }
}
