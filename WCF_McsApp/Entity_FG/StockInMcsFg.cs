﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class mStockArea
    {
        [DataMember]
        public string SA_ID { get; set; }

        [DataMember]
        public string SA_NAME { get; set; }
    }

    [DataContract]
    public class iSetUptoTruck
    {
        [DataMember]
        public string sStockArea { get; set; }

        [DataMember]
        public string sCarTruck { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sUserID { get; set; }
    }

    [DataContract]
    public class mGetCrane
    {
        [DataMember]
        public string CRANE_NO { get; set; }

        [DataMember]
        public string CRANE_DETAIL { get; set; }
    }

    [DataContract]
    public class iSetReceiveToStock
    {
        [DataMember]
        public string sCrane { get; set; }

        [DataMember]
        public string sBarcodeRef { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sUserID { get; set; }
    }

    [DataContract]
    public class iSetUpRecive_Rev
    {
        [DataMember]
        public string sCrane { get; set; }

        [DataMember]
        public string sCarTruck { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sUserID { get; set; }
    }

    [DataContract]
    public class iSetDownRecive_Rev
    {
        [DataMember]
        public string sCrane { get; set; }

        [DataMember]
        public string sBarcodeRef { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public string sUserID { get; set; }
    }


 

    [DataContract]
    public class iDateInputRemind
    {
        [DataMember]
        public string sDate { get; set; }

        [DataMember]
        public int nTypeJob { get; set; }
    }

    [DataContract]
    public class iDateInputSummary
    {
        [DataMember]
        public string sDateStart { get; set; }

        [DataMember]
        public string sDateFinish { get; set; }

        [DataMember]
        public int nTypeJob { get; set; }
    }

    [DataContract]
    public class mSummaryByDate
    {
        [DataMember]
        public List<mGetSummary> GetSummary { get; set; }

        [DataMember]
        public int TOTAL { get; set; }
    }

    [DataContract]
    public class mGetSummary
    {
        [DataMember]
        public string DATE { get; set; }

        [DataMember]
        public string REQ { get; set; }

        [DataMember]
        public string REC { get; set; }

        [DataMember]
        public string REM { get; set; }
    }

    [DataContract]
    public class mRemindByDate
    {
        [DataMember]
        public List<getRemindByDate> GetRemindByDate { get; set; }

        [DataMember]
        public int TOTAL { get; set; }
    }

    [DataContract]
    public class getRemindByDate
    {
        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public string WELD { get; set; }

        [DataMember]
        public string LENGTH { get; set; }

        [DataMember]
        public string WEIGHT { get; set; }
    }


    [DataContract]
    public class mGetStockInDaily
    {
        [DataMember]
        public List<getStockInDaily> GetStockInDaily { get; set; }

        [DataMember]
        public int TOTAL { get; set; }

    }

    [DataContract]
    public class getStockInDaily
    {
        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public string TYPE_NAME { get; set; }

        [DataMember]
        public string SHARP { get; set; }

        [DataMember]
        public string ITEM { get; set; }

        [DataMember]
        public string LENGTH { get; set; }

        [DataMember]
        public string WEIGHT { get; set; }

        [DataMember]
        public string FROM_PLACE { get; set; }

        [DataMember]
        public string FROM_CAR_TRUCK { get; set; }

        [DataMember]
        public string DEST_CRANE { get; set; }

        [DataMember]
        public string PLACE { get; set; }

        [DataMember]
        public string USERS { get; set; }

        [DataMember]
        public string DATES { get; set; }
    }

}