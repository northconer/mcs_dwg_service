﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iUserInGroup
    {
        [DataMember]
        public string sUs_Id { get; set; }
    }

    [DataContract]
    public class mUserInGroup
    {
        [DataMember]
        public string US_ID { get; set; }

        [DataMember]
        public string FULLNAME { get; set; }
    }

    [DataContract]
    public class iLeaveQuotaByYear
    {
        [DataMember]
        public string sUs_Id { get; set; }

        [DataMember]
        public int nYear { get; set; }
    }

    [DataContract]
    public class mLeaveQuotaByYear
    {
        [DataMember]
        public string LEAVE_TYPE_MSG { get; set; }

        [DataMember]
        public string NUMBER_OF_QUOTA { get; set; }

        [DataMember]
        public string NUMBER_OF_QUOTA_USAGE { get; set; }

        [DataMember]
        public float LEAVE_REMAIN { get; set; }

        [DataMember]
        public string LIFETIMES { get; set; }
    }

    [DataContract]
    public class mListYearAndUser
    {
        [DataMember]
        public List<mYear> getYear{ get; set; }

        [DataMember]
        public List<mUserInGroup> getUserIngroup { get; set; }
    }

    [DataContract]
    public class mYear
    {
        [DataMember]
        public int YEAR { get; set; }
    }
}
