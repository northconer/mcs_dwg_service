﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class CheckBarcode
    {
        [DataMember]
        public string barcode { get; set; }

        [DataMember]
        public int process { get; set; }
    }

    [DataContract]
    public class GetBarcodePartActual
    {
        [DataMember]
        public string barcode { get; set; }

        [DataMember]
        public int item_no { get; set; }

        [DataMember]
        public int built_type_id { get; set; }
    }

    [DataContract]
    public class SetBarcodePartActual
    {
        [DataMember]
        public string barcode_part { get; set; }

        [DataMember]
        public int qty { get; set; }

        [DataMember]
        public int actual { get; set; }
    }

    [DataContract]
    public class SetBarcodePartDiaphragm
    {
        [DataMember]
        public string built_code { get; set; }

        [DataMember]
        public string cutting_plan { get; set; }

        [DataMember]
        public int qty { get; set; }

        [DataMember]
        public int actual { get; set; }
    }

    //[DataContract]
    //public class GetSymbol
    //{
    //    [DataMember]
    //    public int process { get; set; }

    //    [DataMember]
    //    public string barcode { get; set; }

    //    [DataMember]
    //    public int item_no { get; set; }

    //    [DataMember]
    //    public int built_type_id { get; set; }
    //}

    //[DataContract]
    //public class SetSymbol
    //{
    //    [DataMember]
    //    public string symbol_id { get; set; }

    //    [DataMember]
    //    public string symbol_name { get; set; }
    //}

    [DataContract]
    public class GetActualPart
    {
        [DataMember]
        public string barcode_part { get; set; }

        [DataMember]
        public string barcode { get; set; }

        [DataMember]
        public int item_no { get; set; }

        [DataMember]
        public int built_type_id { get; set; }

        [DataMember]
        public int pc_id { get; set; }

        [DataMember]
        public string symbol { get; set; }

        [DataMember]
        public string user { get; set; }
    }

    [DataContract]
    public class SetActualPart
    {
        [DataMember]
        public string result { get; set; }

        [DataMember]
        public string message { get; set; }
    }

    [DataContract]
    public class GetBarcodePartActualBox
    {
        [DataMember]
        public string barcode { get; set; }

        [DataMember]
        public int item_no { get; set; }

        [DataMember]
        public int built_type_id { get; set; }

        [DataMember]
        public int pc_id { get; set; }
    }
}