﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class InputLogin
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }
    }

    [DataContract]
    public class SetUserPass
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string OldPassword { get; set; }

        [DataMember]
        public string NewPassword { get; set; }
    }

    [DataContract]
    public class SetDevice
    {
        [DataMember]
        public string Device_id { get; set; }

        [DataMember]
        public string Device_brand { get; set; }

        [DataMember]
        public string Device_model { get; set; }

        [DataMember]
        public string Device_version { get; set; }

        [DataMember]
        public string Device_imei { get; set; }

        [DataMember]
        public char Device_status { get; set; }

        [DataMember]
        public string Device_track { get; set; }

        [DataMember]
        public string Device_ip { get; set; }
    }

    [DataContract]
    public class CheckAuthen
    {
        [DataMember]
        public string sUsername { get; set; }

        [DataMember]
        public string sMn_File_Name { get; set; }
    }
}