﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
   public class InputOtherStep1
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public string sUserID { get; set; }
    }

    public class InputOtherStep2
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int nPd_Rev { get; set; }

        [DataMember]
        public int nProcess { get; set; }
    }

    [DataContract]
    public class InputOtherActualSend
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int nPd_Rev { get; set; }

        [DataMember]
        public int nProcess { get; set; }

        [DataMember]
        public string sUserID { get; set; }

        [DataMember]
        public string sPlace { get; set; }

        [DataMember]
        public string sUser_Actual { get; set; }

        [DataMember]
        public int nResult { get; set; }
    }
}
