﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{
    [DataContract]
    public class BuiltDetailProject
    {
        [DataMember]
        public int PJ_ID { get; set; }

        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public Int16 PD_ITEM { get; set; }

        [DataMember]
        public decimal PD_WEIGHT { get; set; }

        [DataMember]
        public List<GetItem> getItem { get; set; }

        [DataMember]
        public List<GetBuiltType> getBuiltType { get; set; }

        [DataMember]
        public List<GetProcess> getProcess {get; set;}

        [DataMember]
        public int PC_ID { get; set; }
    }

    [DataContract]
    public class GetItem
    {
        [DataMember]
        public Int16 ITEM_NO { get; set; }

        [DataMember]
        public Int16 ITEM_NO_NAME { get; set; }
    }

    [DataContract]
    public class GetBuiltType
    {
        [DataMember]
        public Int16 BUILT_TYPE_ID { get; set; }

        [DataMember]
        public string BUILT_TYPE_NAME { get; set; }
    }

    [DataContract]
    public class GetProcess
    {
        [DataMember]
        public int PC_ID { get; set; }

        [DataMember]
        public string PC_NAME { get; set; }
    }

    [DataContract]
    public class GetBuiltStatus
    {
        [DataMember]
        public int PC_ID {get;set;}

        [DataMember]
        public string  PC_NAME {get;set;}

        [DataMember]
        public string  UG_NAME {get;set;}

        [DataMember]
        public string PA_PLAN_DATE  {get;set;}

        [DataMember]
        public string US_FNAME  {get;set;}

        [DataMember]
        public string  PA_ACTUAL_DATE {get;set;}

        [DataMember]
        public int PC_ID2 { get; set; }

        [DataMember]
        public string PC_NAME2 { get; set; }

        [DataMember]
        public string UG_NAME2 { get; set; }

        [DataMember]
        public string PA_PLAN_DATE2 { get; set; }
    }

}
