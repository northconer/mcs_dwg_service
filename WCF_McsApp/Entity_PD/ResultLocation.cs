﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;


namespace WCF_McsApp
{
    [DataContract]
    public class InputLocation
    {
        [DataMember]
        public string sPlace { get; set; }
    }

    [DataContract]
    public class GetLocationList
    {
        [DataMember]
        public string PLACE { get; set; }

        [DataMember]
        public string PLACE_NAME { get; set; }
    }
}
