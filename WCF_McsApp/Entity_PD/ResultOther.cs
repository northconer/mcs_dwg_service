﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class GetListPcRev
    {
        [DataMember]
        public List<GetOtherProcess> getListPC { get; set; }

        [DataMember]
        public List<GetOtherRev> getListRev { get; set; }

        [DataMember]
        public int PC_ID { get; set; }

        [DataMember]
        public Int16 PD_REV { get; set; }
    }

    [DataContract]
    public class GetOtherProcess
    {
        [DataMember]
        public int PC_ID { get; set; }

        [DataMember]
        public string PC_NAME { get; set; }
    }

    [DataContract]
    public class GetOtherRev
    {
        [DataMember]
        public Int16 PD_REV { get; set; }

        [DataMember]
        public Int16 PD_REV_NAME { get; set; }
    }


    [DataContract]
    public class OtherGetProcessDetail
    {
        [DataMember]
        public int PJ_ID { get; set; }

        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public Int16 PD_ITEM { get; set; }

        [DataMember]
        public Int16 PD_REV { get; set; }

        [DataMember]
        public string UG_NAME { get; set; }

        [DataMember]
        public string PA_PLAN_DATE { get; set; }

        [DataMember]
        public string PA_STATUS { get; set; }

        [DataMember]
        public Int16 PA_STATE { get; set; }

        [DataMember]
        public List<GetOtherUser> getOtherUser { get; set; }
    }

    [DataContract]
    public class GetOtherUser
    {
        [DataMember]
        public string US_ID { get; set; }

        [DataMember]
        public string FULLNAME { get; set; }
    }
}
