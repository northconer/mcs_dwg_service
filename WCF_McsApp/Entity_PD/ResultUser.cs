﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class ResultUser
    {
        [DataMember]
        public string US_ID { get; set; }

        [DataMember]
        public string US_FNAME { get; set; }
    }

    [DataContract]
    public class mConfig
    {
        [DataMember]
        public string CONFIG_KEY { get; set; }

        [DataMember]
        public string CONFIG_VALUE { get; set; }
    }

    [DataContract]
    public class mDivice
    {
        [DataMember]
        public string DIVICE_ID { get; set; }

        [DataMember]
        public string DEVICE_BRAND { get; set; }

        [DataMember]
        public string DEVICE_MODEL { get; set; }

        [DataMember]
        public string DEVICE_VERSION { get; set; }

        [DataMember]
        public string DEVICE_IMEI { get; set; }

        [DataMember]
        public string DEVICE_TRACK { get; set; }

        [DataMember]
        public char DEVICE_STATUS { get; set; }
    }
}
