﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class iRevNoInForm
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nCheck_no { get; set; }
    }

    [DataContract]
    public class mRevNoInForm
    {
        [DataMember]
        public string REV_NO { get; set; }

        [DataMember]
        public string REV_NAME { get; set; }
    }

    [DataContract]
    public class iProjectImage
    {
        [DataMember]
        public int nType { get; set; }
    }

    [DataContract]
    public class mProjectImage
    {
        [DataMember]
        public string IMAGE_NAME { get; set; }

        [DataMember]
        public string POSITION_ID { get; set; }
    }

    [DataContract]
    public class mDirection
    {
        [DataMember]
        public string DRNAME { get; set; }
    }

    [DataContract]
    public class iPosition
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public int nPt_MainType { get; set; }
    }

    [DataContract]
    public class mPosition
    {
        [DataMember]
        public string NT_NAME { get; set; }

        [DataMember]
        public string NT_ID { get; set; }
    }

    [DataContract]
    public class iDescriptions
    {
        [DataMember]
        public int nPt_MainType { get; set; }

        [DataMember]
        public int nNt_Id{ get; set; }
    }

    [DataContract]
    public class mDescriptions
    {
        [DataMember]
        public string DESCRIPTIONS { get; set; }

        [DataMember]
        public string NCR_NO { get; set; }
    }

    [DataContract]
    public class iResponse
    {
        [DataMember]
        public string sNcr_no { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nRev_no { get; set; }
    }

    [DataContract]
    public class mResponse
    {
        [DataMember]
        public string RESPONSE_GROUP { get; set; }

        [DataMember]
        public int DESC_CHK { get; set; }
    }

    [DataContract]
    public class iChProjectDetail
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nRev_no { get; set; }
    }

    [DataContract]
    public class mChProjectDetail
    {
        [DataMember]
        public string PJ_NAME { get; set; }

        [DataMember]
        public string PLAN_DATE { get; set; }

        [DataMember]
        public string PD_CODE { get; set; }

        [DataMember]
        public string PT_MAINTYPE { get; set; }

        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public string UG_NAME { get; set; }

        [DataMember]
        public string ERROR { get; set; }

        [DataMember]
        public int NC_NCR { get; set; }

        [DataMember]
        public string PLACE { get; set; }
    }

    [DataContract]
    public class iNcrCheckAdd
    {
        [DataMember]
        public string sNc_id { get; set; }

        [DataMember]
        public int nOther_no { get; set; }

        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nRev_no { get; set; }

        [DataMember]
        public int nCheck_no { get; set; }

        [DataMember]
        public string sNcr_no { get; set; }

        [DataMember]
        public string sUser { get; set; }

        [DataMember]
        public string sGroup { get; set; }

        [DataMember]
        public string sSug { get; set; }

        [DataMember]
        public string sDirection { get; set; }

        [DataMember]
        public int nDwg_type{ get; set; }

        [DataMember]
        public string sPlanDate { get; set; }

        [DataMember]
        public string sPlanIns { get; set; }

        [DataMember]
        public int? Dwg { get; set; }

        [DataMember]
        public int? Actual { get; set; }

        [DataMember]
        public bool Get_Image { get; set; } //เช็ครูป

    }

    [DataContract]
    public class iNoNcrCheck
    {                            
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nDwg_type{ get; set; }

        [DataMember]
        public string sPlanDate { get; set; }

        [DataMember]
        public string sUser { get; set; }

        [DataMember]
        public string sPlanIns { get; set; }

        [DataMember]
        public int nRev_no { get; set; }

        [DataMember]
        public int nCheck_no { get; set; }

    }

    [DataContract]
    public class iNcrCheckEdit
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nRev_No { get; set; }

        [DataMember]
        public int nCheck_No { get; set; }

        [DataMember]
        public string sNcr_No { get; set; }

        [DataMember]
        public int nOther_no { get; set; }

        [DataMember]
        public string sUsers { get; set; }

        [DataMember]
        public string sPlace { get; set; }

        [DataMember]
        public string sFile { get; set; }

        [DataMember]
        public string sSug { get; set; }

        [DataMember]
        public string sDirection { get; set; }

        [DataMember]
        public string sGroup { get; set; }

        [DataMember]
        public int? Dwg { get; set; }

        [DataMember]
        public int? Actual { get; set; }

        [DataMember]
        public bool Get_Image { get; set; } //เช็ครูป
    }

    [DataContract]
    public class iNcrCheckDelete
    {
        [DataMember]
        public string sNc_Id { get; set; }

        [DataMember]
        public string sNcr_No { get; set; }

        [DataMember]
        public int nOther_no { get; set; }

        [DataMember]
        public string sUser { get; set; }

        [DataMember]
        public string sImage_Before { get; set; }
    }

    [DataContract]
    public class iNcrReport
    {
        [DataMember]
        public string sNc_Id { get; set; }

        [DataMember]
        public string sNc_State { get; set; }
    }

    [DataContract]
    public class mNcrReport
    {
        [DataMember]
        public string NCR_NO { get; set; }

        [DataMember]
        public string DIRECTION { get; set; }

        [DataMember]
        public string POSITION { get; set; }

        [DataMember]
        public string DESCRIPTIONS { get; set; }

        [DataMember]
        public string IMG_BEFORE { get; set; }

        [DataMember]
        public string RESPONSE { get; set; }

        [DataMember]
        public string DWG { get; set; }

        [DataMember]
        public string ACTUAL { get; set; }

        [DataMember]
        public int? OTHER_NO { get; set; }

        [DataMember]
        public string PLACE { get; set; }

        [DataMember]
        public string SUGGEST { get; set; }
    }

    [DataContract]
    public class iNcrType
    {
        [DataMember]
        public string sNcr_No { get; set; }
    }

    [DataContract]
    public class mNcrType
    {
        [DataMember]
        public string NT_ID { get; set; }
    }

    [DataContract]
    public class iConfirmNcr
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public string sBarcode { get; set; }

        [DataMember]
        public int nRev_No { get; set; }

        [DataMember]
        public int nCheck_No { get; set; }

        [DataMember]
        public string sUser { get; set; }
    }

    [DataContract]
    public class iListDataForm
    {
        [DataMember]
        public int nPt_MainType { get; set; }
    }

    [DataContract]
    public class mListDataForm
    {
        [DataMember]
        public List<mProjectImage> getDwgType { get; set; }

        [DataMember]
        public List<mDirection> getDirection { get; set; }
    }

    [DataContract]
    public class igetOther
    {
        [DataMember]
        public string sNc_id { get; set; }

        [DataMember]
        public string sNcr_no { get; set; }
    }

    [DataContract]
    public class mgetOther
    {
        [DataMember]
        public string OTHER_NO { get; set; }
    }
}
