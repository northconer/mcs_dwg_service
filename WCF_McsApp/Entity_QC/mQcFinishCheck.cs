﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WCF_McsApp
{

    [DataContract]
    public class iChecker
    {
        [DataMember]
        public int nTypePc { get; set; }
    }

    [DataContract]
    public class mChecker
    {
        [DataMember]
        public string FULLNAME { get; set; }

        [DataMember]
        public string USERS { get; set; }
    }

    [DataContract]
    public class iNcrReportFN
    {
        [DataMember]
        public int nTypePc { get; set; }

        [DataMember]
        public int nPj_Id { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public string sUser_Check { get; set; }

        [DataMember]
        public string sDate_Edit { get; set; }

        [DataMember]
        public string sPd_Code { get; set; }
    }

    [DataContract]
    public class mNcrReportFN
    {
        [DataMember]
        public string PJ_NAME { get; set; }

        [DataMember]
        public string PD_ITEM { get; set; }

        [DataMember]
        public string REV_NO { get; set; }

        [DataMember]
        public int CHECK_NO { get; set; }

        [DataMember]
        public string PD_CODE { get; set; }

        [DataMember]
        public string USERNAME { get; set; }

        [DataMember]
        public string DATE_START { get; set; }

        [DataMember]
        public string COUNT_NCR { get; set; }

        [DataMember]
        public string NC_NCR { get; set; }

        [DataMember]
        public string BARCODE { get; set; }

        [DataMember]
        public string NC_ID { get; set; }

        [DataMember]
        public string PLACE { get; set; }
    }

    [DataContract]
    public class iNcrCheckFN
    {
        [DataMember]
        public string sNc_Id { get; set; }

        [DataMember]
        public string sUsers { get; set; }
    }
}
