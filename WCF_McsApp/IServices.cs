﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.ComponentModel;

namespace WCF_McsApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.

    [ServiceContract]
    public interface IServices
    {

        #region LOG
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "LogLogin")]
        [Description("LOG - Log User Login")]
        void LogLogin(LogApplication input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserMenu")]
        [Description("Get UserMenu For Android")]
        List<UserMenu> GetUserMenu();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetNewVersion")]
        [Description("Get New Version")]
         mResult GetNewupdate(Newupdate input);


        #endregion

        #region PD
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetConfig")]
        [Description("PD - Config Show Data")]
        List<mConfig> Getconfig();  //mConfig อยู่ใน Userinfo

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetCreateDevice")]
        [Description("PD -  Creat Log Device Mobile")]
        mResult CreateDevice(SetDevice input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Login")]
        [Description("PD - Check Login")]
        ResultUser CheckUsernamePassword(InputLogin input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CheckUserAuthen")]
        [Description("PD - Check User Authen")]
        mResult CheckUserAuthen(CheckAuthen input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CheckGroupAuthen")]
        [Description("PD - Check Group Authen")]
        mResult CheckGroupAuthen(CheckAuthen input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "SetUserPassword")]
        string SetUsernamePassword(SetUserPass input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CheckLocationBarcode")]
        [Description("Check location barcode")]
        Result CheckLocationBarcode(GetCheckLocationBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CheckUserGroupBuilt")]
        [Description("Check user group for send built process")]
        Result CheckUserGroupBuilt(GetCheckUserGroupBuilt input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetProductStatus")]
        [Description("PD - Process Product Status Show Data")]
        GetProductInfo GetProductStatus(InputBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetProductProcess")]
        [Description("PD - Process Product Send Show Process Name")]
        List<GetProcessPD> GetProductProcess();

        [OperationContract]
        [WebInvoke(Method = "POST",ResponseFormat = WebMessageFormat.Json,RequestFormat = WebMessageFormat.Json,UriTemplate = "GetProductSend")]
        [Description("PD - Process Product Send Show Process Name")]
        GetProductSend GetProductSend(InputBarcodeType input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetBuiltProductStep1")]
        [Description("PD - Process Built Beam & Box Show Data Part 1")]
        BuiltDetailProject GetBuiltProjectDetails(InputBuiltProject input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetBuiltProductStep2")]
        [Description("PD - Process Built Beam & Box Show Data Part 2")]
        GetBuiltStatus GetbuiltStatus(InputCheckBuilt input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetBuiltActual")]
        [Description("PD - Process Built Beam & Box Save Data To Database")]
        ResultStatus setBuiltActualSend(InputBuiltActualSend input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetFabDetail_1")]
        [Description("PD - Process Fab Show Data Part 1")]
        FabGetProcess GetFabProcessDetail(InputFabBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFabDetail_2")]
        [Description("PD - Process Fab Show Data Part 2")]
        FabGetProject GetFabProjectDetail(InputFabBarcodePC input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetFabDetail_3")]
        [Description("PD - Process Fab Show Data Part 3")]
        List<FabGetGroupUser> GetFabGroupUser(InputFabGroup input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "SetFabActualSend")]
        [Description("PD - Process Fab Save Data To Database")]
        ResultStatus SetFabActual(InputFabActualSend input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetWeldDetail_1")]
        [Description("PD - Process Weld Show Data Part 1")]
        WeldGetProcess GetWeldProcessDetail(InputWeldBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetWeldDetail_2")]
        [Description("PD - Process Weld Show Data Part 2")]
        WeldGetProject GetWeldProjectDetail(InputWeldBarcodePC input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetWeldDetail_3")]
        [Description("PD - Process Weld Show Data Part 3")]
        List<WeldGetGroupUser> GetWeldGroupUser(InputWeldGroup input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "SetWeldActualSend")]
        [Description("PD - Process Weld Save Data To Database")]
        ResultStatus SetWeldActual(InputWeldActualSend input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetOtherDetail_1")]
        [Description("PD - Process Other Show Data Part 1")]
        GetListPcRev GetOtherProcess(InputOtherStep1 input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetOtherDetail_2")]
        [Description("PD - Process Other Show Data Part 2")]
        OtherGetProcessDetail GetOtherProcessDetail(InputOtherStep2 input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "SetOtherActualSend")]
        [Description("PD - Process Other Save Data To Database")]
        ResultStatus SetOtherActual(InputOtherActualSend input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetRevisePlan_1")]
        [Description("PD - Process Plan Revise Show List Group Name")]
        List<GetGruopRevise> GetGroupReviseInfo();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetRevisePlan_2")]
        [Description("PD - Process Plan Revise Show Data")]
        GetRevisePlanGroup GetReviseInfo(InputReviseGroup input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetReviseProject")]
        [Description("PD - Process Revise Fab & Weld Show Data Part 1")]
        GetReviseRev GetreviseRevNo(InputRevisePJ input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetReviseDetail")]
        [Description("PD - Process Revise Fab & Weld Show Data Part 2")]
        GetReviseDetail GetreviseDetail(InputReviseDT input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "GetReviseGroup")]
        [Description("PD - Process Revise Fab & Weld Show Data Part 3")]
        List<GetReviseGroup> GetreviseGroup(InputGroupRevise input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetReviseActual")]
        [Description("PD - Process Revise Fab & Weld Save Data To Database")]
        ResultStatus SetREviseActual(InputReviseActual input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetNcrRevCheckList")]
        [Description("PD - Process Ncr Show Data Part 1")]
        NcrGetRevCheckList GetNcrRevCheckList(InputNcrBarcodePC input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetNcrProduct")]
        [Description("PD - Process NcrShow Data Part 2")]
        NcrProduct GetDimProduct(InputNcrPD input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetNcrReport")]
        [Description("PD - Process Ncr Show Data Part 3")]
        List<NcrGetReport> GetNcrReport(InputNcrReport input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetNcrPassNcr")]
        [Description("PD - Process Ncr Save Data To Database")]
        ResultStatus SetNcrSend(InputNcrPassNcr input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SetQADimension")]
        [Description("PD - Process Ncr Request Save Data To Database")]
        ResultStatus SetNCR_Request(InputNcrReport input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadImages_PD")]
        [Description("PD -  Upload Images To Folder PD")]
        mResult uploadImages_PD(Stream fileUpload);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetLocationList")]
        [Description("PD - Process Ncr Show Data List Location")]
        List<GetLocationList> getLocationList(InputLocation input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "PD_GetProcessUt")]
        [Description("PD - Process UT")]
        List<getProcessUt> PD_GetProcessUt();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "PD_UTRequest")]
        [Description("PD - UT Request")]
        setRequestReport PD_UTRequest(getRequestReport input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "PD_ReportUTRequest")]
        [Description("PD - Report UT Request")]
        List<setReportUTRequest> PD_ReportUTRequest(getReportUTRequest input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "PD_BarcodePart")]
        [Description("PD - Get barcode part for actual in built up beam process")]
        List<SetBarcodePartActual> PD_BarcodePartActual(GetBarcodePartActual input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "PD_BarcodePartSkinPlate")]
        [Description("PD - Get barcode part for actual in SkinPlate process")]
        List<SetBarcodePartActual> PD_BarcodePartSkinPlate(GetBarcodePartActualBox input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "PD_BarcodePartDiaPhragm")]
        [Description("PD - Get barcode part for actual in Diaphragm process")]
        List<SetBarcodePartDiaphragm> PD_BarcodePartDiaphragm(GetBarcodePartActualBox input);

        //[OperationContract]
        //[WebInvoke(
        //    Method = "POST",
        //    RequestFormat = WebMessageFormat.Json,
        //    ResponseFormat = WebMessageFormat.Json,
        //    UriTemplate = "PD_Symbol")]
        //[Description("PD - Get Symbol for actual in built up beam process")]
        //List<SetSymbol> PD_Symbol(GetSymbol input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "PD_ActualPart")]
        [Description("PD - Set actual part in built up beam process")]
        SetActualPart PD_ActualPart(GetActualPart input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "PD_CheckBarcodePart")]
        [Description("PD - Check barcode part")]
        int PD_CheckBarcodePart(CheckBarcode input);

	#endregion

        #region Qc
        //PC : QC PLAN CHECK
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "PCgetProjectName")]
        [Description("QC - Process Plan/Summary Status Show List Project Name ")]
        List<mProject> PCgetProjectName(iProject input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "PCgetProjectItem")]
        [Description("QC - Process Plan/Summary Status Show List Item ")]
        List<mItemPj> PCgetProjectItem(iItemPj input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "PCgetProjectRevno")]
        [Description("QC - Process Plan/Summary Status Show List Revise")]
        List<mRevNoPlanCheck> PCgetProjectRevno(iRevNoPlanCheck input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PCgetGroupFab")]
        [Description("QC - Process Plan/Summary Status Show List Group Fab")]
        List<mGroupFab> PCgetGroupFab();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PCgetGroupWeld")]
        [Description("QC - Process Plan/Summary Status Show List Group Weld")]
        List<mGroupWeld> PCgetGroupWeld();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PCgetStatus")]
        [Description("QC - Process Plan/Summary Status Show List Status")]
        List<mStatus> PCgetStatus();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "PCgetProjectShowListCheck")]
        [Description("QC - Process Plan/Summary Status Show Data Report")]
        List<mPjShowListCheck> PCgetProjectShowListCheck(iPjShowListCheck input);

        //IF : IN FORM NCR
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "IFgetListDataForm")]
        [Description("QC - Process Inform Ncr Show List DWG Type & Direction")]
        mListDataForm IFgetListDataForm(iListDataForm input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetProjectRevNo")]
        [Description("QC - Process Inform Ncr Show List Revise")]
        List<mRevNoInForm> IFgetProjectRevNo(iRevNoInForm input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetProjectDetail")]
        [Description("QC - Process Inform Ncr Show Detail Of project")]
        mChProjectDetail IFgetProjectDetail(iChProjectDetail input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetPositionList")]
        [Description("QC - Process Inform Ncr Show List Positions")]
        List<mPosition> IFgetPositionList(iPosition input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetDescriptionsNcr")]
        [Description("QC - Process Inform Ncr Show List Descriptions")]
        List<mDescriptions> IFgetDescriptionsNcr(iDescriptions input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetResponse")]
        [Description("QC - Process Inform Ncr Show Data Response")]
        mResponse IFgetResponse(iResponse input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetAddNcrCheck")]
        [Description("QC - Process Inform Ncr Save Data To Database : Add Ncr")]
        mResult IFsetAddNcrCheck(iNcrCheckAdd input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetNoNcrCheck")]
        [Description("QC - Process Inform Ncr Save Data To Database : No Ncr")]
        mResult IFsetNoNcrCheck(iNoNcrCheck input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetNcrReport")]
        [Description("QC - Process Inform Ncr Show Data Ncr Report")]
        List<mNcrReport> IFgetNcrReport(iNcrReport input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetNcrType")]
        mNcrType IFgetNcrType(iNcrType input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetEditNcrCheck")]
        [Description("QC - Process Inform Ncr Save Data To Database : Edit Ncr")]
        mResult IFsetEditNcrCheck(iNcrCheckEdit input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetDeleteNcrCheck")]
        [Description("QC - Process Inform Ncr Save Data To Database : Delete Ncr")]
        mResult IFsetDeleteNcrCheck(iNcrCheckDelete input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFsetConfirmNcr")]
        [Description("QC - Process Inform Ncr Save Data To Database : Confirm Ncr")]
        mResult IFsetConfirmNcr(iConfirmNcr input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "IFgetOther")]
        [Description("QC - Process Inform Ncr Show Other Of Ncr")]
        mgetOther IFgetOther(igetOther input);

        //RC : Qc Recheck
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCgetDataRecheck")]
        [Description("QC - Process Recheck Show Data Part 1")]
        mDataRecheck RCgetDataRecheck(iDataRecheck input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCgetNcrReport")]
        [Description("QC - Process Recheck Show Data Report")]
        List<mNcrReportRC> RCgetNcrReport(iNcrReportRC input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCsetRecheckOK")]
        [Description("QC - Process Inform Ncr Save Data To Database : Recheck OK")]
        mResult RCsetRecheckOK(iRecheckOK input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCsetRecheckNotOK")]
        [Description("QC - Process Inform Ncr Save Data To Database : Recheck Not OK")]
        mResult RCsetRecheckNotOK(iRecheckNotOK input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "RCsetRecheckDel")]
        mResult RCsetRecheckDel(iRecheckDel input);

        //FN : QC Final
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNgetProjectName")]
        [Description("QC - Process Confirm Show List Project Name")]
        List<mProject> FNgetProjectName(iProject input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNgetProjectItem")]
        [Description("QC - Process Confirm Show List Item")]
        List<mItemPj> FNgetProjectItem(iItemPj input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNgetChecker")]
        [Description("QC - Process Confirm Show List User Check")]
        List<mChecker> FNgetChecker(iChecker input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNgetNcrReport")]
        [Description("QC - Process Confirm Show Data Report Ncr")]
        List<mNcrReportFN> FNgetNcrReport(iNcrReportFN input);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "FNsetNcrQcCheck")]
        [Description("QC - Process Confirm Ncr Save Data To Database : Confirm Ncr")]
        mResult FNsetNcrQcCheck(iNcrCheckFN input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadImages_QC")]
        [Description("QC - Upload Images To Folder QC")]
        mResult uploadImages_QC(Stream fileUpload);
        #endregion

        #region FG
        #region Stock
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStock_GetShelfName")]
        [Description("FG - Stock Get Shelf Name")]
        List<mShelfName> GetShelfName();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStock_GetProductOnShelf")]
        [Description("FG - Stock Get Product On Shelf")]
        mProductOnShelf  GetProductOnShelf(iShelfName input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStock_GetProductSwap")]
        [Description("FG - Stock Get DataSwap")]
        List<mgetShowdataSwap> GetDataSwap(igetShowdataSwap input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgPaint_SetUpdateSwap")]
        [Description("FG - Stock Update DataSwap ")]
        mResult SetUpdateSwap(iupdatebarcodeSwap input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStock_SetNumberSort")]
        [Description("FG - Stock Set Number Sort ")]
        mResult SetNumberSort(iNumberSort input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStock_SetNumberUpdate")]
        [Description("FG - Stock Set Number Update")]
        mResult SetNumberUpdate(iNumberUpdate input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStock_SetStockOut")]
        [Description("FG - Stock Out")]
        mResult StockOut(iStockOut input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStock_SetStockIn")]
        [Description("FG - Stock In")]
        mResult StockIn(iStockIn input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStock_ProductSort")]
        [Description("FG - Stock In")]
        mBarcodeSort BarcodeSort(iBarcodeSort input);


        #endregion

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgCheckLoginUser")]
        [Description("FG - Login")]
        mLoginMcsFg CheckLoginUserMcsFg(iLoginMcsFg input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgCheckUserMenu")]
        [Description("FG - User Menu")]
        List<mUserCheckMenu> CheckUserMenu(iUserCheckMenu input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetProductStatus")]
        [Description("FG - Stock Info Get Product Status")]
        mGetProductStatus GetProductStatusFG(iBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetProductPlaceInStock")]
        [Description("FG - Stock Info Get Product Place In Stock")]
        List<mGetProductPlaceInStock> GetProductPlaceInStock(iProjectNameAndItem input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetStockArea")]
        [Description("FG - Stock In Get Stock Area")]
        List<mStockArea> GetStockArea();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgSetUptoTruck")]
        [Description("FG - Stock In Set Up To Trauck")]
        mResult SetUptoTruck(iSetUptoTruck input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetCrane")]
        [Description("FG - Stock In Get Crane")]
        List<mGetCrane> GetCrane();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgSetReceiveToStock")]
        [Description("FG - Stock In Set Receive To Stock")]
        mResult SetReceiveToStock(iSetReceiveToStock input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgSetUpReceive")]
        [Description("FG - Stock In Revise Set Up Receive")]
        mResult SetUpReceive_Rev(iSetUpRecive_Rev input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgSetDownReceive")]
        [Description("FG - Stock In Revise Set Down Receive")]
        mResult SetDownReceive_Rev(iSetDownRecive_Rev input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetShowSummaryByDate")]
        [Description("FG - Stock In Summary By Date")]
        mSummaryByDate GetShowSummaryByDate(iDateInputSummary input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetRemindByDate")]
        [Description("FG - Stock In Remine By Date")]
        mRemindByDate GetRemindByDate(iDateInputRemind input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgGetStockInDaily")]
        [Description("FG - Stock In Dairy")]
        mGetStockInDaily GetStockInDaily(iDateInputRemind input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_SetOnTruck")]
        [Description("FG - Stock Out Set On Truck")]
        mResult SetOnTruck(iSetOnTruck input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_SetOutCom")]
        [Description("FG - Stock Out Set Out Com")]
        mResult SetOutCom(iSetOutCom input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_SetUpReceive")]
        [Description("FG - Stock Out Revise Set Up Receive")]
        mResult SetOutUpReceive(iSetOutUpReceive input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_SetDownReceive")]
        [Description("FG - Stock Out Revise Set Down Receive")]
        mResult SetOutDownReceive(iSetOutDownReceive input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_GetSummaryByDate")]
        [Description("FG - Stock Out Get Summary By Date")]
        mGetSummaryByDateStockOut GetSummaryByDateStockOut(iGetSummaryByDate_SO input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_GetRemindByDate")]
        [Description("FG - Stock Out Remine By Date")]
        mGetRemindByDateStockOut GetRemindByDateStockOut(iGetRemindDate input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_GetDaily")]
        [Description("FG - Stock Out Dairy")]
        mGetStockOutDaily GetStockOutDaily(iGetRemindDate input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgStockOut_PrintDocOnTruckNoDoc")]
        [Description("FG - Stock Out Print Document")]
        mResult PrintDocOnTruckNoDo(iPrintDocOnTruckNoDo input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgPaint_ShowDataBarcode")]
        [Description("FG - Paint Show Data Barcode")]
        mPaintDataShow GetPainShowDataBarcode(iPaintDataShowBarcode input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgPaint_ShowDataReport")]
        [Description("FG -Paint Show Data Report")]
        List< mPaintDataReport> GetPaintShowDataRePort(iPaintDataReport input);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgPaint_GetProject")]
        [Description("FG - Paint Get Project Name")]
        List<mProjectPaint> GetProject();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgPaint_GetItem")]
        [Description("FG - Paint Get Item")]
        List<mItemPaint> GetTtemPaint(igetItem input);

        
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgPaint_SetInsertPaint")]
        [Description("FG - Paint Set Insert ")]
        mResult SetInsertPaint(iInsertPaint input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsFgUser_Authen")]
        [Description("FG - User Authen ")]
        mResult CheckAuthen(mCheckAuthen input); 
        

        #endregion

        #region HR
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserInGroup")]
        [Description("HR - Show List User In Group")]
        mListYearAndUser getUserInGroup(iUserInGroup input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetMasterLeaveQuotaByYear")]
        [Description("HR - Show Data Leave Quota")]
        List<mLeaveQuotaByYear> getMasterLeaveQuotaByYear(iLeaveQuotaByYear input);
        #endregion

        #region CMMS
        #region Config
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "CheckMTDep")]
        Result CheckMTDep(SetLogin input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetLogin")]
        SetLogin GetLogin(GetLogin input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetEmployeeByEmpCode")]
        setEmployeeByEmpCode GetEmployeeByEmpCode(getEmployeeByEmpCode input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "EmailNotification")]
        void EmailNotification(emailNotification input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UploadImages_CMMS")]
        Result UploadImages_CMMS(Stream file);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "EmailContract")]
        void GetEmailContract(GetEmailContract input);

        #endregion

        #region DDL
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DDL_AssetType")]
        List<SetAssetType> AssetType(GetAssetType input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DDL_AssetStatus")]
        List<SetAssetStatus> AssetStatus(GetAssetStatus input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DDL_Evaluate")]
        List<SetEvaluate> Evaluate(GetEvaluate input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DDL_Priority")]
        List<SetPriority> Priority(GetPriority input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DDL_RepairCause")]
        List<DDL_SetRepairCause> DDL_RepairCause(DDL_GetRepairCause input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DDL_RepairStatus")]
        List<SetRepairStatus> RepairStatus(GetRepairStatus input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_RequestByRepair")]
        List<SEL_SetRequestByRepair> SEL_RequestByRepair(SEL_GetRequestByRepair input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DDL_Team")]
        List<SetTeam> Team(GetTeam input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DDL_WorkClass")]
        List<SetWorkClass> WorkClass(GetWorkClass input);

        #endregion

        #region DEL

        #region DEL_Request
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DEL_Request")]
        queryResult DEL_Request(DEL_GetRequest input);

        #endregion

        #endregion

        #region INS

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "INS_DocumentByUploadImg")]
        queryResult INS_DocumentByUploadImg(INS_DocumentByUploadImg input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "INS_Repair")]
        queryResult INS_Repair(INS_GetRepair input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "INS_RequestByRepair")]
        queryResult INS_RequestByRepair(INS_GetRequestByRepair input);

        #endregion

        #region SEL
        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_Asset")]
        List<SEL_SetAsset> SEL_Asset(SEL_GetAsset input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_AssetByType")]
        List<SEL_SetAssetByType> SEL_AssetByType(SEL_GetAssetByType input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_AssetType")]
        List<SEL_SetAssetType> SEL_AssetType(SEL_GetAssetType input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_Component")]
        List<SEL_SetComponent> SEL_Component(SEL_GetComponent input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_Location")]
        List<SEL_SetLocation> SEL_Location(SEL_GetLocation input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_OperatorById")]
        List<SEL_SetOperatorById> SEL_OperatorById(SEL_GetOperatorById input);

        [OperationContract]
        [WebInvoke(
            Method = "GET",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_Repair")]
        List<SEL_SetRepair> SEL_Repair();

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_RepairByAdd")]
        List<SEL_SetRepairByAdd> SEL_RepairByAdd(SEL_GetRepairByAdd input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_RepairById")]
        SEL_SetRepairById SEL_RepairById(SEL_GetRepairById input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_RepairByUserForAssign")]
        List<SEL_SetRepairByUserForAssign> SEL_RepairByUserForAssign(SEL_GetRepairByUserForAssign input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_RepairByUserForEvaluate")]
        List<SEL_SetRepairByUserForEvaluate> SEL_RepairByUserForEvaluate(SEL_GetRepairByUserForEvaluate input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_RepairByWorkList")]
        List<SEL_SetRepairByWorkList> SEL_RepairByWorkList(SEL_GetRepairByWorkList input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SEL_RepairWorkListByRpId")]
        List<SEL_SetRepairWorkListByRpId> SEL_RepairWorkListByRpId(SEL_GetRepairWorkListByRpId input);
        #endregion

        #region UPD

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_DocumentByRpId")]
        queryResult UPD_DocumentByRpId(UPD_DocumentByRpId input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_DocumentByUploadImgFail")]
        queryResult UPD_DocumentByUploadImgFail(UPD_DocumentByUploadImgFail input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_Repair")]
        queryResult UPD_Repair(UPD_GetRepair input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByAdminCancel")]
        queryResult UPD_RepairByAdminCancel(UPD_GetRepairByAdminCancel input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByAssign")]
        queryResult UPD_RepairByAssign(UPD_GetRepairByAssign input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByAssignNone")]
        queryResult UPD_RepairByAssignNone(UPD_GetRepairByAssignNone input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByEvaluate")]
        queryResult UPD_RepairByEvaluate(UPD_GetRepairByEvaluate input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByOperatorOpen")]
        queryResult UPD_RepairByOperatorOpen(UPD_GetRepairByOperatorOpen input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByOepratorRequest")]
        queryResult UPD_RepairByOperatorRequest(UPD_GetRepairByOperatorRequest input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByOperatorSent")]
        queryResult UPD_RepairByOperatorSent(UPD_GetRepairByOperatorSent input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByUploadFailed")]
        queryResult UPD_RepairByUploadFailed(UPD_GetRepairByUploadFailed input);

        [OperationContract]
        [WebInvoke(
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UPD_RepairByUserCancel")]
        queryResult UPD_RepairByUserCancel(UPD_GetRepairByUserCancel input);
        #endregion
        #endregion

        #region Drawing Step A

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsDrawing_StepA")]
        [Description("Dwg - Sum Schedule ")]
        List<iShowSchedule> McsDrawing_StepA(iShowSchedule input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsDrawing_SelectDB")]
        [Description("Dwg - Select Database ")]
        List<iShowDatabase> McsDrawing_SelectDB(iShowDatabase input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsDrawing_SelectAllDB")]
        [Description("Dwg - Select All Database ")]
        List<iShowAllData> McsDrawing_SelectAllDB(iShowAllData input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsDrawing_SetSchedule")]
        [Description("Dwg - Set Schedule ")]
        List<iSetSchedule> McsDrawing_SetSchedule(iSetSchedule input);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "McsDrawing_ShowReportDwgSchedule")]
        [Description("Dwg - Show Report Dwg Schedule ")]
        List<iShowReport> McsDrawing_ShowReportDwgSchedule(iShowReport input);

        #endregion

    }


    //-----------------------------------------------------------------

    [DataContract]
    public class mResult
    {
        [DataMember]
        public string RESULT { get; set; }

        [DataMember]
        public string EXCEPTION { get; set; }
    }
}
