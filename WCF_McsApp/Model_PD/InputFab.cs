﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{


    [DataContract]
    public class InputFabBarcode
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

    }

    [DataContract]
    public class InputFabBarcodePC
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int nProcess { get; set; }
    }

    [DataContract]
    public class InputFabGroup
    {
        [DataMember]
        public string sUG_Name { get; set; }
    }

    [DataContract]
    public class InputFabActualSend
    {
        [DataMember]
        public int nPJ_ID { get; set; }

        [DataMember]
        public int nPd_Item { get; set; }

        [DataMember]
        public int nProcess { get; set; }

        [DataMember]
        public int nVal { get; set; }

        [DataMember]
        public string sUserActual { get; set; }

        [DataMember]
        public string sPlace { get; set; }

        [DataMember]
        public string sUserID { get; set; }

        [DataMember]
        public int Result { get; set; }
    }
}
