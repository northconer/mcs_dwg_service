﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace WCF_McsApp
{
    [DataContract]
    public class NcrGetRevCheckList
    {
        [DataMember]
        public List<GetlistRevNo> GetRevNo { get; set; }

        [DataMember]
        public List<GetlistCheckNo> GetCheckNo { get; set; }
    }

    [DataContract]
    public class GetlistRevNo
    {
        [DataMember]
        public int REV_NO { get; set; }

        [DataMember]
        public string REV_NAME { get; set; }
    }

    [DataContract]
    public class GetlistCheckNo
    {
        [DataMember]
        public int CHECK_NO { get; set; }

        [DataMember]
        public int CHECK_NAME { get; set; }
    }

    [DataContract]
    public class NcrProduct
    {
        [DataMember]
        public string PROJECT { get; set; }

        [DataMember]
        public string PT_ITEMTYPE { get; set; }

        [DataMember]
        public Int16 PD_ITEM { get; set; }

        [DataMember]
        public string PD_DWG { get; set; }

        [DataMember]
        public string PD_CODE { get; set; }

    }

    [DataContract]
    public class NcrGetReport
    {
        [DataMember]
        public string GROUP_NAME { get; set; }

        [DataMember]
        public string NCR_NO { get; set; }

        [DataMember]
        public string NT_NAME { get; set; }

        [DataMember]
        public string DESCRIPTIONS { get; set; }

        [DataMember]
        public string NC_ID { get; set; }

        [DataMember]
        public int  OTHER_NO { get; set; }

        [DataMember]
        public string IMG_BEFORE { get; set; }
    }
}
