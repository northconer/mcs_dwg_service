﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;

namespace WCF_McsApp
{
    [DataContract]
    public class GetProductInfo
    {
        [DataMember]
        public string FPROJ { get; set; }

        [DataMember]
        public string TYPE { get; set; }

        [DataMember]
        public string SPROJ { get; set; }

        [DataMember]
        public Int16 SHARP { get; set; }

        [DataMember]
        public Int16 ITEM { get; set; }

        [DataMember]
        public string CODE { get; set; }

        [DataMember]
        public string DWG { get; set; }

        [DataMember]
        public string SIZE { get; set; }

        [DataMember]
        public decimal LENGTH { get; set; }

        [DataMember]
        public decimal WEIGHT { get; set; }

        [DataMember]
        public List<GetProductStatus> getProductStatus { get; set; }
    }

    [DataContract]
    public class GetProductStatus
    {
        [DataMember]
        public string PC_NAME { get; set; }

        [DataMember]
        public string PA_ACTUAL_DATE { get; set; }

        [DataMember]
        public string ANAME { get; set; }
    }

    [DataContract]
    public class GetProcessPD
    {
        [DataMember]
        public int TYPE_ID { get; set; }

        [DataMember]
        public string TYPE_NAME { get; set; }
    }


    [DataContract]
    public class GetProductSend
    {
        [DataMember]
        public string PJ_NAME { get; set; }

        [DataMember]
        public List<GetListProductSend> getListProductSend { get; set; }
    }

    [DataContract]
    public class GetListProductSend
    {
        [DataMember]
        public string PC_NAME { get; set; }

        [DataMember]
        public string TIMES { get; set; }

        [DataMember]
        public string NAMES { get; set; }

        [DataMember]
        public int ACTUAL { get; set; }
    }
}
