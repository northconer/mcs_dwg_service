﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data;
using System.Web.DataAccess;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using HttpMultipartParser;
using System.Web.Configuration;

namespace WCF_McsApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IServices
    {
        string ConnMcsApp = ConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString;
        string ConnMcsQc = ConfigurationManager.ConnectionStrings["McsQCConnectionString"].ConnectionString;
        string ConnMcsHrMs = ConfigurationManager.ConnectionStrings["McsHrMsConnectionString"].ConnectionString;

        #region PD
        #region เก็บค่า config และ log ผู้ใช้งานเข้าสู่ระบบ McsPdPc
        public List<mConfig> Getconfig()
        {
            McsAppDataContext dc = new McsAppDataContext();

            List<mConfig> results = new List<mConfig>();

            foreach (PDPC_config item in dc.PDPC_configs.OrderBy(s => s.config_key))
            {
                results.Add(new mConfig
                {
                    CONFIG_KEY = item.config_key,
                    CONFIG_VALUE = item.config_value
                });
            }
            return results;
        }

        public mResult CreateDivice(SetDevice input)
        {
            mResult result = new mResult();
            DateTime now = DateTime.Now;

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                PDPC_device icheck = dc.PDPC_devices.Where(s => s.device_id == input.Device_id).FirstOrDefault();

                if (icheck == null)
                {
                    PDPC_device device = new PDPC_device()
                    {
                        device_id = new string(input.Device_id.Take(20).ToArray()),
                        device_brand = new string(input.Device_brand.ToUpper().Take(10).ToArray()),
                        device_model = new string(input.Device_model.ToUpper().Take(10).ToArray()),
                        device_version = new string(input.Device_version.Take(20).ToArray()),
                        device_imei = new string(input.Device_imei.Take(20).ToArray()),
                        device_time = (DateTime)(now),
                        device_status = input.Device_status
                    };

                    dc.PDPC_devices.InsertOnSubmit(device);
                }
                else
                {
                    icheck.device_id = new string(input.Device_id.Take(20).ToArray());
                    icheck.device_brand = new string(input.Device_brand.ToUpper().Take(10).ToArray());
                    icheck.device_model = new string(input.Device_model.ToUpper().Take(10).ToArray());
                    icheck.device_version = new string(input.Device_version.Take(20).ToArray());
                    icheck.device_imei = new string(input.Device_imei.Take(20).ToArray());
                    icheck.device_time = (DateTime)(now);
                    icheck.device_status = input.Device_status;
                }

                dc.SubmitChanges();

                result.RESULT = "true";
                result.EXCEPTION = "";

                return result;
            }
            catch (Exception ex)
            {
                result.RESULT = "false";
                result.EXCEPTION = ex.Message;

                return result;
            }
        }
        #endregion

        #region Login ป้อน User and Password
        public ResultUser CheckUsernamePassword(InputLogin input)
        {
            ResultUser UserInfo = new ResultUser();

            string check = "";

            object[] prm = { input.Username, input.Password, 0 };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_LOGIN", prm);

            while (reader.Read())
            {
                check = reader["RESULT"].ToString();
            }

            if (check == "T")
            {
                using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command1 = new SqlCommand())
                    {
                        command1.Connection = connection;
                        command1.CommandText = "SELECT US_ID ,US_FNAME + ' ' + US_LNAME AS US_FNAME FROM McsAppCenter.dbo.USERS WHERE US_ID=@US_ID";
                        command1.CommandType = CommandType.Text;
                        command1.Parameters.Add("@US_ID", SqlDbType.VarChar, 7).Value = input.Username;

                        SqlDataReader dr = command1.ExecuteReader();

                        while (dr.Read())
                        {
                            UserInfo.US_ID = dr["US_ID"].ToString();
                            UserInfo.US_FNAME = dr["US_FNAME"].ToString();
                        }
                    }

                    connection.Close();
                }
            }
            else
            {
                UserInfo.US_ID = null;
                UserInfo.US_FNAME = null;
            }
            return UserInfo;
        }

        //แก้ไขรหัสผ่าน
        public string SetUsernamePassword(SetUserPass input)
        {
            object[] prm = { input.Username, input.OldPassword, input.NewPassword };

            string returnMessage = "";
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SET_UPDATE_CHANGE_PASSWORD", prm);
            while (reader.Read())
            {
                returnMessage = (string)reader["RETURN_MESSAGE"];
            }

            return returnMessage;
        }
        #endregion

        public mResult CheckUserAuthen(CheckAuthen input)
        {
            mResult result = new mResult();

            mcsappcenterDataContext db = new mcsappcenterDataContext();

            int ua_auth = 0;
            string subfilename = input.sMn_File_Name.Substring(6);

            if (subfilename == "status")
            {
                ua_auth = 1;
            }
            else if (subfilename == "inform_ncr" || subfilename == "recheck_ncr")
            {
                ua_auth = 2;
            }
            else if (subfilename == "confirm_ncr")
            {
                ua_auth = 4;
            }

            V306_Group us_check = db.V306_Groups.Where(ua => ua.US_ID == input.sUsername && ua.MN_FILE_NAME == input.sMn_File_Name && ua.UA_AUTH >= ua_auth).FirstOrDefault();

            if (us_check == null)
            {
                result.RESULT = "false";
                result.EXCEPTION = "";
            }
            else
            {
                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            return result;
        }

        public mResult CheckGroupAuthen(CheckAuthen input)
        {
            mResult result = new mResult();

            mcsappcenterDataContext db = new mcsappcenterDataContext();


            V306_Group gu_check = db.V306_Groups.Where(ua => ua.US_ID == input.sUsername && ua.MN_FILE_NAME == input.sMn_File_Name).FirstOrDefault();

            if (gu_check == null)
            {
                result.RESULT = "false";
                result.EXCEPTION = "";
            }
            else
            {
                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            return result;
        }

        #region SP_GET_PRODUCT_STATUS เช็คสถานะชิ้นงาน
        public GetProductInfo GetProductStatus(InputBarcode input)
        {
            GetProductInfo GetProductStatus = new GetProductInfo();
            GetProductStatus.getProductStatus = new List<GetProductStatus>();

            object[] prm = { input.nPJ_ID, input.nPd_Item };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_PRODUCT_STATUS", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetProductStatus.FPROJ = (string)reader["FPROJ"];
                    GetProductStatus.TYPE = (string)reader["TYPE"];
                    GetProductStatus.SPROJ = (string)reader["SPROJ"];
                    GetProductStatus.SHARP = (Int16)reader["SHARP"];
                    GetProductStatus.ITEM = (Int16)reader["ITEM"];
                    GetProductStatus.CODE = (string)reader["CODE"];
                    GetProductStatus.DWG = (string)reader["DWG"];
                    GetProductStatus.SIZE = reader["SIZE"].ToString();
                    GetProductStatus.LENGTH = (decimal)reader["LENGTH"];
                    GetProductStatus.WEIGHT = (decimal)reader["WEIGHT"];
                }
            }
            else
            {
                GetProductStatus.FPROJ = "-";
                GetProductStatus.TYPE = "-";
                GetProductStatus.SPROJ = "-";
                GetProductStatus.SHARP = 0;
                GetProductStatus.ITEM = 0;
                GetProductStatus.CODE = "-";
                GetProductStatus.DWG = "-";
                GetProductStatus.SIZE = "-";
                GetProductStatus.LENGTH = 0;
                GetProductStatus.WEIGHT = 0;
            }


            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetProductStatus.getProductStatus.Add(new GetProductStatus
                        {
                            PC_NAME = (string)reader["PC_NAME"],
                            PA_ACTUAL_DATE = (string)reader["PA_ACTUAL_DATE"],
                            ANAME = (string)reader["ANAME"]
                        });
                    }

                }
                else
                {
                    GetProductStatus.getProductStatus.Add(new GetProductStatus
                    {
                        PC_NAME = "-",
                        PA_ACTUAL_DATE = "-",
                        ANAME = "-"
                    });
                }
            }
            return GetProductStatus;
        }
        #endregion

        #region GET_PRODUCT_SEND เช็คสถานะส่งงาน
        public List<GetProcessPD> GetProductProcess()
        {
            return new List<GetProcessPD>
            {
                new GetProcessPD {TYPE_ID = 1, TYPE_NAME = "Built Box" },
                new GetProcessPD {TYPE_ID = 2, TYPE_NAME = "Built Beam" },
                new GetProcessPD {TYPE_ID = 3, TYPE_NAME = "Fab" },
                new GetProcessPD {TYPE_ID = 4, TYPE_NAME = "Weld" }
            };
        }

        public GetProductSend GetProductSend(InputBarcodeType input)
        {
            GetProductSend GetProductSend = new GetProductSend();
            GetProductSend.getListProductSend = new List<GetListProductSend>();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.Type };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_PRODUCT_SEND", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetProductSend.PJ_NAME = (string)reader["pj_name"];
                }
            }
            else
            {
                GetProductSend.PJ_NAME = "-";
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetProductSend.getListProductSend.Add(new GetListProductSend
                        {
                            PC_NAME = (string)reader["PC_NAME"],
                            TIMES = (string)reader["TIMES"],
                            NAMES = (string)reader["NAMES"],
                            ACTUAL = (int)reader["ACTUAL"]
                        });
                    }
                }
                else
                {
                    GetProductSend.getListProductSend.Add(new GetListProductSend
                    {
                        PC_NAME = "-",
                        TIMES = "-",
                        NAMES = "-",
                        ACTUAL = 0
                    });
                }
            }

            return GetProductSend;
        }
        #endregion

        #region GET_BUILT_PRODUCT_DETAIL_STEP_1
        public BuiltDetailProject GetBuiltProjectDetails(InputBuiltProject input)
        {
            BuiltDetailProject BuiltInfo = new BuiltDetailProject();

            BuiltInfo.getItem = new List<GetItem>();

            BuiltInfo.getBuiltType = new List<GetBuiltType>();

            BuiltInfo.getProcess = new List<GetProcess>();

            BuiltInfo.PC_ID = new int();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.Users };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BUILT_PRODUCT_DETAIL_STEP_1", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    BuiltInfo.PJ_ID = (int)reader["PJ_ID"];
                    BuiltInfo.PROJECT = (string)reader["PROJECT"];
                    BuiltInfo.PD_ITEM = (Int16)reader["PD_ITEM"];
                    BuiltInfo.PD_WEIGHT = (decimal)reader["PD_WEIGHT"];
                }
            }
            else
            {
                BuiltInfo.PJ_ID = 0;
                BuiltInfo.PROJECT = "-";
                BuiltInfo.PD_ITEM = 0;
                BuiltInfo.PD_WEIGHT = 0;
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getItem.Add(new GetItem
                        {
                            ITEM_NO = (Int16)reader["ITEM_NO"],
                            ITEM_NO_NAME = (Int16)reader["ITEM_NO_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getItem.Add(new GetItem
                    {
                        ITEM_NO = 0,
                        ITEM_NO_NAME = 0
                    });
                }
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getBuiltType.Add(new GetBuiltType
                        {
                            BUILT_TYPE_ID = (Int16)reader["BUILT_TYPE_ID"],
                            BUILT_TYPE_NAME = (string)reader["BUILT_TYPE_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getBuiltType.Add(new GetBuiltType
                    {
                        BUILT_TYPE_ID = 0,
                        BUILT_TYPE_NAME = "-"
                    });
                }
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BuiltInfo.getProcess.Add(new GetProcess
                        {
                            PC_ID = (int)reader["PC_ID"],
                            PC_NAME = (string)reader["PC_NAME"]
                        });
                    }
                }
                else
                {
                    BuiltInfo.getProcess.Add(new GetProcess
                    {
                        PC_ID = 0,
                        PC_NAME = "-"
                    });
                }
            }
            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    BuiltInfo.PC_ID = (int)reader["PC_ID"];
                }
            }
            return BuiltInfo;
        }
        #endregion

        #region GET_BUILT_PRODUCT_DETAIL_STEP_2
        public GetBuiltStatus GetbuiltStatus(InputCheckBuilt input)
        {
            GetBuiltStatus getBuiltStatus = new GetBuiltStatus();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nItem_no, input.nBuilt_Type, input.nProcess };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_BUILT_PRODUCT_DETAIL_STEP_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    getBuiltStatus.PC_ID = (int)reader["PC_ID"];
                    getBuiltStatus.PC_NAME = (string)reader["PC_NAME"];
                    getBuiltStatus.UG_NAME = (string)reader["UG_NAME"];
                    getBuiltStatus.US_FNAME = (string)reader["US_FNAME"];
                    getBuiltStatus.PA_PLAN_DATE = (string)reader["PA_PLAN_DATE"];
                    getBuiltStatus.PA_ACTUAL_DATE = (string)reader["PA_ACTUAL_DATE"];
                }
            }
            else
            {
                getBuiltStatus.PC_ID = 0;
                getBuiltStatus.PC_NAME = "-";
                getBuiltStatus.UG_NAME = "-";
                getBuiltStatus.US_FNAME = "-";
                getBuiltStatus.PA_PLAN_DATE = "-";
                getBuiltStatus.PA_ACTUAL_DATE = "-";
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        getBuiltStatus.PC_ID2 = (int)reader["PC_ID"];
                        getBuiltStatus.PC_NAME2 = (string)reader["PC_NAME"];
                        getBuiltStatus.UG_NAME2 = (string)reader["UG_NAME"];
                        getBuiltStatus.PA_PLAN_DATE2 = Convert.ToDateTime(reader["PA_PLAN_DATE"]).ToString("yyyy-MM-dd");
                    }
                }
                else
                {
                    getBuiltStatus.PC_ID2 = 0;
                    getBuiltStatus.PC_NAME2 = "-";
                    getBuiltStatus.UG_NAME2 = "-";
                    getBuiltStatus.PA_PLAN_DATE2 = "-";
                }
            }
            return getBuiltStatus;
        }

        #endregion

        #region SP_CHECK_SET_BUILT_ACTUAL ส่งงาน built
        public ResultStatus setBuiltActualSend(InputBuiltActualSend input)
        {
            ResultStatus ResultStatus = new ResultStatus();
            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nItem_no, input.nBuilt_Type, input.nProcess, input.sUser_work, input.sPlace, input.sUser_id, input.nResult };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_BUILT_SENT", prm);
            while (reader.Read())
            {
                ResultStatus.RESULT = (string)reader["ERROR"];
            }
            return ResultStatus;
        }
        #endregion


        #region SP_GET_FAB_PRODUCT_DETAIL_1
        public FabGetProcess GetFabProcessDetail(InputFabBarcode input)
        {
            FabGetProcess FabGetProcess = new FabGetProcess();
            FabGetProcess.getProcessFab = new List<GetProcessFab>();

            McsAppDataContext dc = new McsAppDataContext();

            var query = (from f in dc.FAB_SUMs
                         where f.pc_id != 81000 && f.pj_id == input.nPJ_ID && f.pd_item == input.nPd_Item
                         orderby f.pc_id
                         select new { PC_ID = f.pc_id, PC_NAME = dc.sf_ProcessName(f.pc_id.ToString()) }).FirstOrDefault();

            if (query == null)
            {
                FabGetProcess.getProcessFab.Add(new GetProcessFab
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }
            else
            {
                var fab_pc = (from f in dc.FAB_SUMs
                              where f.pc_id != 81000 && f.pj_id == input.nPJ_ID && f.pd_item == input.nPd_Item
                              select new { PC_ID = f.pc_id, PC_NAME = dc.sf_ProcessName(f.pc_id.ToString()) });

                foreach (var pc_fab in fab_pc)
                {
                    FabGetProcess.getProcessFab.Add(new GetProcessFab
                    {
                        PC_ID = (int)pc_fab.PC_ID,
                        PC_NAME = (string)pc_fab.PC_NAME
                    });
                }
            }
            return FabGetProcess;
        }
        #endregion

        #region SP_GET_FAB_PRODUCT_DETAIL_2
        public FabGetProject GetFabProjectDetail(InputFabBarcodePC input)
        {
            FabGetProject FabGetProject = new FabGetProject();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nProcess };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_FAB_PRODUCT_DETAIL_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    FabGetProject.PJ_ID = (int)reader["PJ_ID"];
                    FabGetProject.PD_ITEM = (Int16)reader["PD_ITEM"];
                    FabGetProject.PJ_NAME = (string)reader["PJ_NAME"];
                    FabGetProject.PT_NAME = (string)reader["PT_NAME"];
                    FabGetProject.UG_NAME = (string)reader["UG_NAME"];
                    FabGetProject.PLANDATE = (string)reader["PLANDATE"];
                    FabGetProject.PC_ID = (int)reader["PC_ID"];
                    FabGetProject.PC_NAME = (string)reader["PC_NAME"];
                    FabGetProject.FAB_PLAN = (int)reader["FAB_PLAN"];
                    FabGetProject.FAB_ACTUAL = (int)reader["FAB_ACTUAL"];
                }
            }
            else
            {
                FabGetProject.PJ_ID = 0;
                FabGetProject.PD_ITEM = 0;
                FabGetProject.PJ_NAME = "-";
                FabGetProject.PT_NAME = "-";
                FabGetProject.UG_NAME = "-";
                FabGetProject.PLANDATE = "-";
                FabGetProject.PC_ID = 0;
                FabGetProject.PC_NAME = "-";
                FabGetProject.FAB_PLAN = 0;
                FabGetProject.FAB_ACTUAL = 0;
            }
            return FabGetProject;
        }
        #endregion

        #region SP_GET_FAB_PRODUCT_DETAIL_3
        public List<FabGetGroupUser> GetFabGroupUser(InputFabGroup input)
        {
            object[] prm = { input.sUG_Name };
            List<FabGetGroupUser> listFabGroupUser = new List<FabGetGroupUser>();
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_FAB_PRODUCT_DETAIL_3", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    listFabGroupUser.Add(new FabGetGroupUser
                    {
                        US_ID = (string)reader["US_ID"],
                        FULLNAME = reader["FULLNAME"].ToString()
                    });
                }
            }
            else
            {
                listFabGroupUser.Add(new FabGetGroupUser
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return listFabGroupUser;
        }
        #endregion

        #region SP_SET_FAB_SENT_REPORT ส่งงาน Fab
        public ResultStatus SetFabActual(InputFabActualSend input)
        {
            ResultStatus REsultstatus = new ResultStatus();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nProcess, input.nVal, input.sUserActual, input.sPlace, input.sUserID, input.Result };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_FAB_SENT_REPORT", prm);

            while (reader.Read())
            {
                REsultstatus.RESULT = (string)reader["ERROR"];
            }
            return REsultstatus;
        }
        #endregion


        #region SP_GET_WELD_PRODUCT_DETAIL_1
        public WeldGetProcess GetWeldProcessDetail(InputWeldBarcode input)
        {
            WeldGetProcess WeldGetProcess = new WeldGetProcess();
            WeldGetProcess.getProcessWeld = new List<GetProcessWeld>();

            McsAppDataContext dc = new McsAppDataContext();

            var query = (from w in dc.WELD_SUMs
                         where w.pc_id != 82000 && w.pj_id == input.nPJ_ID && w.pd_item == input.nPd_Item
                         orderby w.pc_id
                         select new { PC_ID = w.pc_id, PC_NAME = dc.sf_ProcessName(w.pc_id.ToString()) }).FirstOrDefault();

            if (query == null)
            {
                WeldGetProcess.getProcessWeld.Add(new GetProcessWeld
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }
            else
            {
                var weld_pc = (from w in dc.WELD_SUMs
                               where w.pc_id != 82000 && w.pj_id == input.nPJ_ID && w.pd_item == input.nPd_Item
                               orderby w.pc_id
                               select new { PC_ID = w.pc_id, PC_NAME = dc.sf_ProcessName(w.pc_id.ToString()) });

                foreach (var pc_weld in weld_pc)
                {
                    WeldGetProcess.getProcessWeld.Add(new GetProcessWeld
                    {
                        PC_ID = (int)pc_weld.PC_ID,
                        PC_NAME = (string)pc_weld.PC_NAME
                    });
                }
            }
            return WeldGetProcess;
        }
        #endregion

        #region SP_GET_WELD_PRODUCT_DETAIL_2
        public WeldGetProject GetWeldProjectDetail(InputWeldBarcodePC input)
        {
            WeldGetProject WeldGetProject = new WeldGetProject();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nProcessID };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_WELD_PRODUCT_DETAIL_2", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    WeldGetProject.PJ_ID = (int)reader["PJ_ID"];
                    WeldGetProject.PD_ITEM = (Int16)reader["PD_ITEM"];
                    WeldGetProject.PJ_NAME = (string)reader["PJ_NAME"];
                    WeldGetProject.PT_NAME = (string)reader["PT_NAME"];
                    WeldGetProject.UG_NAME = (string)reader["UG_NAME"];
                    WeldGetProject.PLANDATE = (string)reader["PLANDATE"];
                    WeldGetProject.PC_ID = (int)reader["PC_ID"];
                    WeldGetProject.PC_NAME = (string)reader["PC_NAME"];
                    WeldGetProject.WELD_PLAN = (int)reader["WELD_PLAN"];
                    WeldGetProject.WELD_ACTUAL = (int)reader["WELD_ACTUAL"];
                }
            }
            else
            {
                WeldGetProject.PJ_ID = 0;
                WeldGetProject.PD_ITEM = 0;
                WeldGetProject.PJ_NAME = "-";
                WeldGetProject.PT_NAME = "-";
                WeldGetProject.UG_NAME = "-";
                WeldGetProject.PLANDATE = "-";
                WeldGetProject.PC_ID = 0;
                WeldGetProject.PC_NAME = "-";
                WeldGetProject.WELD_PLAN = 0;
                WeldGetProject.WELD_ACTUAL = 0;
            }
            return WeldGetProject;
        }
        #endregion

        #region SP_GET_WELD_PRODUCT_DETAIL_3
        public List<WeldGetGroupUser> GetWeldGroupUser(InputWeldGroup input)
        {
            object[] prm = { input.sUG_Name };
            List<WeldGetGroupUser> listWeltGroupUser = new List<WeldGetGroupUser>();
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_WELD_PRODUCT_DETAIL_3", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    listWeltGroupUser.Add(new WeldGetGroupUser
                    {
                        US_ID = (string)reader["US_ID"],
                        FULLNAME = (string)reader["FULLNAME"]
                    });
                }
            }
            else
            {
                listWeltGroupUser.Add(new WeldGetGroupUser
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return listWeltGroupUser;
        }
        #endregion

        #region SP_SET_WELD_SENT_REPORT ส่งงาน Weld
        public ResultStatus SetWeldActual(InputWeldActualSend input)
        {
            ResultStatus ResultSTatus = new ResultStatus();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nProcess, input.nVal, input.sUserActual, input.sPlace, input.sUserID, input.Result };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_WELD_SENT_REPORT", prm);
            while (reader.Read())
            {
                ResultSTatus.RESULT = (string)reader["ERROR"];
            }
            return ResultSTatus;
        }
        #endregion


        #region SP_M_GET_OTHER_PROCESS_DETAIL_STEP_1
        public GetListPcRev GetOtherProcess(InputOtherStep1 input)
        {
            GetListPcRev oTherGet = new GetListPcRev();
            oTherGet.getListPC = new List<GetOtherProcess>();
            oTherGet.getListRev = new List<GetOtherRev>();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.sUserID };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_OTHER_PROCESS_DETAIL_STEP_1", prm);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    oTherGet.getListPC.Add(new GetOtherProcess
                    {
                        PC_ID = (int)reader["PC_ID"],
                        PC_NAME = (string)reader["PC_NAME"]
                    });
                }
            }
            else
            {
                oTherGet.getListPC.Add(new GetOtherProcess
                {
                    PC_ID = 0,
                    PC_NAME = "-"
                });
            }

            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        oTherGet.getListRev.Add(new GetOtherRev
                        {
                            PD_REV = (Int16)reader["PD_REV"],
                            PD_REV_NAME = (Int16)reader["PD_REV_NAME"]
                        });
                    }
                }
                else
                {
                    oTherGet.getListRev.Add(new GetOtherRev
                    {
                        PD_REV = 0,
                        PD_REV_NAME = 0
                    });
                }
            }

            if (reader.Read())
            {
                while (reader.Read())
                {
                    oTherGet.PC_ID = (int)reader["PC_ID"];
                    oTherGet.PD_REV = (Int16)reader["PD_REV"];
                }
            }
            return oTherGet;
        }
        #endregion

        #region SP_M_GET_OTHER_PROCESS_DETAIL_STEP_2
        public OtherGetProcessDetail GetOtherProcessDetail(InputOtherStep2 input)
        {
            OtherGetProcessDetail OtherGEtProcess = new OtherGetProcessDetail();
            OtherGEtProcess.getOtherUser = new List<GetOtherUser>();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nPd_Rev, input.nProcess };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_M_GET_OTHER_PROCESS_DETAIL_STEP_2", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    OtherGEtProcess.PJ_ID = (int)reader["PJ_ID"];
                    OtherGEtProcess.PROJECT = (string)reader["PROJECT"];
                    OtherGEtProcess.PD_ITEM = (Int16)reader["PD_ITEM"];
                    OtherGEtProcess.PD_REV = (Int16)reader["PD_REV"];
                    OtherGEtProcess.UG_NAME = (string)reader["UG_NAME"];
                    OtherGEtProcess.PA_PLAN_DATE = (string)reader["PA_PLAN_DATE"];
                    OtherGEtProcess.PA_STATUS = (string)reader["PA_STATUS"];
                    OtherGEtProcess.PA_STATE = (Int16)reader["PA_STATE"];
                }
            }
            else
            {
                OtherGEtProcess.PJ_ID = 0;
                OtherGEtProcess.PROJECT = "-";
                OtherGEtProcess.PD_ITEM = 0;
                OtherGEtProcess.PD_REV = 0;
                OtherGEtProcess.UG_NAME = "-";
                OtherGEtProcess.PA_PLAN_DATE = "-";
                OtherGEtProcess.PA_STATUS = "-";
                OtherGEtProcess.PA_STATE = 0;
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        OtherGEtProcess.getOtherUser.Add(new GetOtherUser
                        {
                            US_ID = (string)reader["US_ID"],
                            FULLNAME = (string)reader["FULLNAME"]
                        });
                    }
                }
                else
                {
                    OtherGEtProcess.getOtherUser.Add(new GetOtherUser
                    {
                        US_ID = "-",
                        FULLNAME = "-"
                    });
                }

            }
            return OtherGEtProcess;
        }
        #endregion

        #region SP_SET_OTHER_ACTUAL_SEND ส่งงานอื่นๆ
        public ResultStatus SetOtherActual(InputOtherActualSend input)
        {
            ResultStatus reSultsTatus = new ResultStatus();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nPd_Rev, input.nProcess, input.sUserID, input.sPlace, input.sUser_Actual, input.nResult };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_OTHER_SENT", prm);
            while (reader.Read())
            {
                reSultsTatus.RESULT = (string)reader["RESULT"];
            }
            return reSultsTatus;
        }
        #endregion


        #region SP_GET_REVISE_PLAN_GROUP  //แสดงกรุ๊ป
        public List<GetGruopRevise> GetGroupReviseInfo()
        {
            McsAppDataContext dc = new McsAppDataContext();

            List<GetGruopRevise> GroupData = new List<GetGruopRevise>();

            var item = ((from Fab in dc.V301_FabRevs select Fab.ug_name)
                .Union(from Weld in dc.V301_WeldRevs select Weld.ug_name));
            foreach (string items in item)
            {
                GroupData.Add(new GetGruopRevise()
                {
                    UG_NAME = items
                });
            }
            return GroupData;
        }

        public GetRevisePlanGroup GetReviseInfo(InputReviseGroup input) //แสดงแผนงานประกอบ - เชื่อม
        {
            McsAppDataContext dc = new McsAppDataContext();

            GetRevisePlanGroup getRevisePlan = new GetRevisePlanGroup();
            getRevisePlan.getRevise = new List<GetRevisePlan>();

            var FabWeld = ((from f in dc.V301_FabRevs where f.ug_name == input.sUG_Name select new { f.pj_id, f.pd_item, f.mp_sname, f.pt_ItemType, f.pj_sharp, f.ug_name, RevDate = f.revDate })
            .Union(from w in dc.V301_WeldRevs where w.ug_name == input.sUG_Name select new { w.pj_id, w.pd_item, w.mp_sname, w.pt_ItemType, w.pj_sharp, w.ug_name, RevDate = w.revDateWeld }));

            foreach (var items in FabWeld)
            {
                getRevisePlan.getRevise.Add(new GetRevisePlan()
                {
                    PD_ITEM = items.pd_item,
                    MP_SNAME = items.mp_sname,
                    PT_ITEMTYPE = items.pt_ItemType,
                    PJ_SHARP = (Int16)items.pj_sharp,
                    UG_NAME = items.ug_name,
                    REVDATE = items.RevDate
                });
            }

            var total = FabWeld.Count();

            {
                getRevisePlan.TOTAL = total;
            }
            return getRevisePlan;
        }
        #endregion


        #region SP_GET_REVISE_REV_NO
        public GetReviseRev GetreviseRevNo(InputRevisePJ input)
        {
            GetReviseRev GetreviseREv = new GetReviseRev();
            GetreviseREv.getRevno = new List<GetRevno>();
            GetreviseREv.getProcessRev = new List<GetProcessRev>();

            McsAppDataContext dc = new McsAppDataContext();

            REV_PLAN icheck = dc.REV_PLANs.Where(r => r.PJ_ID == input.nPJ_ID && r.ITEM == input.nPd_Item).FirstOrDefault();

            if (icheck != null)
            {
                var query_rev = (from r in dc.REV_PLANs where r.PJ_ID == input.nPJ_ID && r.ITEM == input.nPd_Item orderby r.REV_NO select r.REV_NO).Distinct();
                foreach (var items in query_rev)
                {
                    GetreviseREv.getRevno.Add(new GetRevno
                    {
                        REV_NO = (int)items
                    });
                }
            }
            else
            {
                GetreviseREv.getRevno.Add(new GetRevno
                {
                    REV_NO = 0
                });
            }

            REV_PROCESS icheck_pc = dc.REV_PROCESSes.OrderBy(pc => pc.PC_ID).FirstOrDefault();

            if (icheck_pc != null)
            {
                var query_pc = (from p in dc.REV_PROCESSes orderby p.PC_ID select new { p.PC_NAME, p.PC_ID }).Distinct();
                foreach (var item in query_pc)
                {
                    GetreviseREv.getProcessRev.Add(new GetProcessRev
                    {
                        PROCESS_NAME = item.PC_NAME.ToString(),
                        PC_ID = (int)item.PC_ID
                    });
                }
            }
            else
            {
                GetreviseREv.getProcessRev.Add(new GetProcessRev
                {
                    PROCESS_NAME = "-",
                    PC_ID = 0
                });
            }
            return GetreviseREv;
        }
        #endregion

        #region SP_GET_REVISE_PLAN_DETAIL
        public GetReviseDetail GetreviseDetail(InputReviseDT input)
        {
            GetReviseDetail GetReviseFab = new GetReviseDetail();

            object[] prm = { input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_REVISE_PLAN_DETAIL", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetReviseFab.PJ_ID = (int)reader["PJ_ID"];
                    GetReviseFab.PROJECT = (string)reader["PROJECT"];
                    GetReviseFab.ITEM = (Int16)reader["ITEM"];
                    GetReviseFab.REV_NO = (int)reader["REV_NO"];
                    GetReviseFab.DESIGN_CHANGE = (string)reader["DESIGN_CHANGE"];
                    GetReviseFab.PC_ID = (int)reader["PC_ID"];
                    GetReviseFab.PROCESS = (string)reader["PROCESS"];
                    GetReviseFab.TYPES = (string)reader["TYPES"];
                    GetReviseFab.PLAN_GROUP = (string)reader["PLAN_GROUP"];
                    GetReviseFab.PLAN_ID = (int)reader["PLAN_ID"];
                    GetReviseFab.PLAN_DATE = (string)reader["PLAN_DATE"];
                    GetReviseFab.PLAN = (int)reader["PLAN"];
                }

                if (reader.NextResult())
                {

                    if (reader.NextResult())
                    {
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                GetReviseFab.ACTUAL = (int)reader["ACTUAL"];
                            }
                        }
                    }
                }
            }
            else
            {
                GetReviseFab.PJ_ID = 0;
                GetReviseFab.PROJECT = "-";
                GetReviseFab.ITEM = 0;
                GetReviseFab.REV_NO = 0;
                GetReviseFab.DESIGN_CHANGE = "-";
                GetReviseFab.PC_ID = 0;
                GetReviseFab.PROCESS = "-";
                GetReviseFab.TYPES = "-";
                GetReviseFab.PLAN_GROUP = "-";
                GetReviseFab.PLAN_ID = 0;
                GetReviseFab.PLAN_DATE = "-";
                GetReviseFab.PLAN = 0;
            }
            return GetReviseFab;
        }
        #endregion

        #region SP_GET_USER_IN_GROUP
        public List<GetReviseGroup> GetreviseGroup(InputGroupRevise input)
        {
            List<GetReviseGroup> GetReviseGroup = new List<GetReviseGroup>();

            object[] prm = { input.nUG_ID, input.sUG_Name };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_USER_IN_GROUP", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetReviseGroup.Add(new GetReviseGroup
                    {
                        US_ID = (string)reader["US_ID"],
                        FULLNAME = (string)reader["FULLNAME"]
                    });
                }
            }
            else
            {
                GetReviseGroup.Add(new GetReviseGroup
                {
                    US_ID = "-",
                    FULLNAME = "-"
                });
            }
            return GetReviseGroup;
        }
        #endregion

        #region SP_SET_REVISE_ACTUAL
        public ResultStatus SetREviseActual(InputReviseActual input)
        {
            ResultStatus SetReviseActual = new ResultStatus();
            int check = 0;

            object[] prm = { input.sUserID, input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_CHECK_GROUP", prm);
            while (reader.Read())
            {
                check = (int)reader["RESULT"];
            }

            if (check > 0)
            {
                object[] prm2 = { input.nPJ_ID, input.nPd_Item, input.nRev_no, input.nProcess, input.sUserActual, input.nActual_QTY, input.sUserID, input.QTY };
                var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_REVISE_ACTUAL", prm2);
                while (reader2.Read())
                {
                    SetReviseActual.RESULT = (string)reader2["RESULT"];
                }
            }
            else
            {
                SetReviseActual.RESULT = "ไม่มีสิทธ์ส่งงานข้ามกรุ๊ป";
            }
 
            return SetReviseActual;
        }
        #endregion


        #region Get NCR REV_NO CHECK_NO LIST ลิสต์ Rev_no และ check_no
        public NcrGetRevCheckList GetNcrRevCheckList(InputNcrBarcodePC input)
        {
            NcrGetRevCheckList GetNcrRevCheckList = new NcrGetRevCheckList();
            GetNcrRevCheckList.GetRevNo = new List<GetlistRevNo>();
            GetNcrRevCheckList.GetCheckNo = new List<GetlistCheckNo>();

            object[] prm = { input.sBarcode, input.nType_PC };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_NCR_REV_CHECK_NO_LIST", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    GetNcrRevCheckList.GetRevNo.Add(new GetlistRevNo
                    {
                        REV_NO = (int)reader["REV_NO"],
                        REV_NAME = (string)reader["REV_NAME"]
                    });
                }
            }
            else
            {
                GetNcrRevCheckList.GetRevNo.Add(new GetlistRevNo
                {
                    REV_NO = 0,
                    REV_NAME = "-"
                });
            }
            if (reader.NextResult())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GetNcrRevCheckList.GetCheckNo.Add(new GetlistCheckNo
                        {
                            CHECK_NO = ((int)reader["CHECK_NO"] <= 0) ? 1 : (int)reader["CHECK_NO"],
                            CHECK_NAME = ((int)reader["CHECK_NAME"] <= 0) ? 1 : (int)reader["CHECK_NAME"]
                        });
                    }
                }
                else
                {
                    GetNcrRevCheckList.GetCheckNo.Add(new GetlistCheckNo
                    {
                        CHECK_NO = 1,
                        CHECK_NAME = 1
                    });
                }


            }
            return GetNcrRevCheckList;
        }
        #endregion

        #region SP_GET_DIM_PRODUCT   NCR แสดงรายละเอียดโปรดักส์
        public NcrProduct GetDimProduct(InputNcrPD input)
        {
            NcrProduct GetNcrProduct = new NcrProduct();

            McsAppDataContext dc = new McsAppDataContext();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            var query_dp = (from pd in dc.Products
                            join pt in dc.Product_Types on pd.pt_id equals pt.pt_id
                            where pd.pj_id == project && pd.pd_item == items
                            select new { pj_name = dc.sf_sname(project.ToString()), pt.pt_ItemType, pd.pd_item, pd.pd_dwg, pd.pd_code }).FirstOrDefault();

            if (query_dp != null)
            {
                GetNcrProduct.PROJECT = (string)query_dp.pj_name;
                GetNcrProduct.PT_ITEMTYPE = (string)query_dp.pt_ItemType;
                GetNcrProduct.PD_ITEM = (Int16)query_dp.pd_item;
                GetNcrProduct.PD_DWG = (string)query_dp.pd_dwg;
                GetNcrProduct.PD_CODE = (string)query_dp.pd_code;
            }
            else
            {
                GetNcrProduct.PROJECT = "-";
                GetNcrProduct.PT_ITEMTYPE = "-";
                GetNcrProduct.PD_ITEM = 0;
                GetNcrProduct.PD_DWG = "-";
                GetNcrProduct.PD_CODE = "-";
            }
            return GetNcrProduct;
        }
        #endregion

        #region GET NCR REPORT DESC ลิสต์รายการ ncr
        public List<NcrGetReport> GetNcrReport(InputNcrReport input)
        {
            List<NcrGetReport> NcrGetReport = new List<NcrGetReport>();
            string sNc_id = input.nPC_ID + input.sBarcode + "0" + input.nRev_No + input.nCheck_No;
            string sNc_state = "2";
            string sNd_state = "1";
            string sGroup_name = "";

            object[] us_id = { input.sUS_ID };
            var group_name = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "sp_Group", us_id);
            while (group_name.Read())
            {
                sGroup_name = (string)group_name["ug_name"];
            }

            object[] prm = { sNc_id, sNc_state, sNd_state, sGroup_name, input.nPC_ID, input.sBarcode, input.nRev_No };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    NcrGetReport.Add(new NcrGetReport
                    {
                        GROUP_NAME = (string)reader["GROUP_NAME"],
                        NCR_NO = (string)reader["NCR_NO"],
                        NT_NAME = (string)reader["NT_NAME"],
                        DESCRIPTIONS = (string)reader["DESCRIPTIONS"],
                        NC_ID = (string)reader["NC_ID"],
                        OTHER_NO = (int)reader["OTHER_NO"],
                        IMG_BEFORE = (string)reader["IMG_BEFORE"]
                    });
                }
            }
            else
            {

                NcrGetReport.Add(new NcrGetReport
                {
                    GROUP_NAME = "-",
                    NCR_NO = "-",
                    NT_NAME = "-",
                    DESCRIPTIONS = "-",
                    NC_ID = "-",
                    OTHER_NO = 0,
                    IMG_BEFORE = "No.jpg"
                });
            }
            return NcrGetReport;
        }
        #endregion

        #region ส่งงาน  NCR
        public ResultStatus SetNcrSend(InputNcrPassNcr input)
        {
            DateTime now = DateTime.Now;
            McsQcDataContext dc = new McsQcDataContext();
            ResultStatus Result = new ResultStatus();

            NCR_DIM_CHECK items = dc.NCR_DIM_CHECKs.Where(nc => nc.NC_ID == input.sNC_ID && nc.NCR_NO == input.sNcr_No && nc.OTHER_NO == input.nOther_No).FirstOrDefault();

            try
            {
                if (input.getImage == false)
                {
                    items.ND_STATE = 3;
                    items.IMG_AFTER = "No.jpg";
                    items.USERS = input.sUS_ID;
                    items.TIMES = (DateTime)(now);
                    items.USER_UPDATE = input.sUS_ID;
                    items.TIME_UPDATE = (DateTime)(now);
                    dc.SubmitChanges();

                    Result.RESULT = "T";

                    return Result;
                }
                else
                {
                    string img_before = "";

                    var img_name = from ncr in dc.NCR_DIM_CHECKs.Where(nc => nc.NC_ID == input.sNC_ID && nc.NCR_NO == input.sNcr_No && nc.OTHER_NO == input.nOther_No) select ncr.IMG_BEFORE;
                    foreach (string item in img_name)
                    {
                        img_before = item;
                    }

                    if (img_before == "No.jpg")
                    {
                        items.ND_STATE = 3;
                        items.IMG_AFTER = input.sNC_ID + input.sNcr_No + "-" + input.nOther_No + ".jpg";
                        items.USERS = input.sUS_ID;
                        items.TIMES = (DateTime)(now);
                        items.USER_UPDATE = input.sUS_ID;
                        items.TIME_UPDATE = (DateTime)(now);
                        dc.SubmitChanges();

                        Result.RESULT = "T";

                        return Result;
                    }
                    else
                    {
                        items.ND_STATE = 3;
                        items.IMG_AFTER = img_before;
                        items.USERS = input.sUS_ID;
                        items.TIMES = (DateTime)(now);
                        items.USER_UPDATE = input.sUS_ID;
                        items.TIME_UPDATE = (DateTime)(now);
                        dc.SubmitChanges();

                        Result.RESULT = "T";

                        return Result;
                    }
                }
            }
            catch (Exception ex)
            {
                Result.RESULT = ex.Message;

                return Result;
            }
        }
        #endregion


        #region  แจ้งตรวจ Dimension
        public ResultStatus SetNCR_Request(InputNcrReport input)
        {
            ResultStatus Result = new ResultStatus();

            object[] prm = { input.nPC_ID, input.sBarcode, input.nRev_No, input.nCheck_No, input.sPlace, input.sUS_ID };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_QA_DIMENSION_AD", prm);

            while (reader.Read())
            {
                Result.RESULT = (string)reader["RESULT"];
            }
            return Result;
        }
        #endregion

        public mResult uploadImages_PD(Stream fileUpload)
        {
            mResult result = new mResult();

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                string servPath = "";
                string fileSize = "";

                int point = 0;
                int width = 0;
                int height = 0;

                foreach (PDPC_config items in dc.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_AFTER")) servPath = items.config_value;
                    if (items.config_key.Equals("PICTURE")) fileSize = items.config_value;
                }

                if (fileSize != "")
                {
                    point = fileSize.IndexOf("x");
                    width = Convert.ToInt32(fileSize.Substring(0, point));
                    height = Convert.ToInt32(fileSize.Substring(point + 1));
                }

                //------------------------------------------------------------------------------------------------------

                var parser = new MultipartFormDataParser(fileUpload);

                var username = parser.Parameters["username"].Data;
                var barcode = parser.Parameters["barcode"].Data;
                var otherno = parser.Parameters["otherno"].Data;
                var imagename = parser.Parameters["imagename"].Data;

                var file = parser.Files.FirstOrDefault();

                Stream stream = file.Data;

                //------------------------------------------------------------------------------------------------------

                if (!Directory.Exists(servPath)) Directory.CreateDirectory(servPath);

                FileStream targetStream = null;
                Stream sourceStream = stream;

                string fileName = imagename + "-" + otherno + "-X.jpeg";
                string filePath = Path.Combine(servPath, fileName);

                using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    const int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    int totalBytes = 0;

                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        totalBytes += count;

                        targetStream.Write(buffer, 0, count);
                    }

                    targetStream.Close();

                    sourceStream.Close();
                }

                resizeImage(filePath, width, height);

                result.RESULT = "true";
                result.EXCEPTION = "";
            }
            catch (Exception ex)
            {
                result.RESULT = "false";
                result.EXCEPTION = ex.Message;
            }

            return result;
        }


        private byte[] StreamToByte(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];

            using (MemoryStream ms = new MemoryStream())
            {
                int read;

                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }


        private void resizeImage(string path, int width, int height)
        {
            Bitmap tmp = new Bitmap(path);
            Bitmap bmp = new Bitmap(tmp, width, height);

            bmp.Save(path.Substring(0, path.Length - 7) + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            tmp.Dispose();
            bmp.Dispose();

            tmp = null;
            bmp = null;

            File.Delete(path);
        }

        #region พื้นที่
        public List<GetLocationList> getLocationList(InputLocation input)
        {
            List<GetLocationList> GetLocationList = new List<GetLocationList>();

            object[] prm = { input.sPlace };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_LOCATION_LIST", prm);

            while (reader.Read())
            {
                GetLocationList.Add(new GetLocationList
                {
                    PLACE = (string)reader["PLACE"],
                    PLACE_NAME = (string)reader["PLACE_NAME"]
                });
            }
            return GetLocationList;
        }
        #endregion

        #region UT Request
        public setRequestReport PD_UTRequest(getRequestReport input)
        {
            setRequestReport result = new setRequestReport();

            object[] obj = { input.pj_id, input.pd_item, input.item_process, input.user_request, input.place };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_SET_UT_REQUEST_REPORT", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.query = reader["ERROR"].ToString();
                }
            }

            return result;
        }
        #endregion

        #region Report UT Request
        public List<getProcessUt> PD_GetProcessUt()
        {
            return new List<getProcessUt>
            {
                new getProcessUt {process_id = "G", process_name  = "GMAW" },
                new getProcessUt {process_id = "S", process_name = "SAW" },
                new getProcessUt {process_id = "E", process_name = "ESW" },
            };
        }

        public List<setReportUTRequest> PD_ReportUTRequest(getReportUTRequest input)
        {
            List<setReportUTRequest> list = new List<setReportUTRequest>();

            object[] obj = { input.pj_id, input.pd_item, input.item_process, 1 };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_REPORT_UT_REQUEST", obj);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new setReportUTRequest
                    {
                        pj_id = reader["PJ_ID"].ToString(),
                        pj_name = reader["PJ_NAME"].ToString(),
                        pd_item = reader["PD_ITEM"].ToString(),
                        pd_code = reader["PD_CODE"].ToString(),
                        pd_qty = reader["PD_QTY"].ToString(),
                        pd_dwg = reader["PD_DWG"].ToString(),
                        pd_size = reader["PD_SIZE"].ToString(),
                        place = reader["PLACE"].ToString(),
                        barcode = reader["BARCODE"].ToString(),
                        pa_no = reader["PA_NO"].ToString(),
                        item_process = reader["ITEM_PROCESS"].ToString(),
                        pc_name = reader["PC_NAME"].ToString(),
                        user_request = reader["USER_REQUEST"].ToString(),
                        request_name = reader["REQUEST_NAME"].ToString(),
                        request_date = reader["REQUEST_DATE"].ToString(),
                        ug_name = reader["UG_NAME"].ToString(),
                        ut_status = reader["UT_STATUS"].ToString()
                    });
                }
            }

            return list;
        }
        #endregion

        #endregion

        #region QC
        // Qc Plan Check----------------------------------------------------------------------------------------------
        #region List ชื่อโปรเจค
        public List<mProject> PCgetProjectName(iProject input)
        {
            List<mProject> results = new List<mProject>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT  MCSAPP.DBO.SF_FULLNAME(P.PJ_ID) PJ_NAME,P.PJ_ID 
                                            FROM 
                                            (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                            UNION 
                                            SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                            GROUP BY P.PJ_ID,P.ITEM) P 
				                            WHERE NOT EXISTS
              			                    (SELECT *
              			                    FROM NCR_CHECK NC    
						                    WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
              			                    AND P.PJ_ID = CAST(LEFT(NC.BARCODE,5) AS INT)  AND P.PD_ITEM = CAST(RIGHT(NC.BARCODE,4) AS INT)   AND P.REV_NO = NC.REV_NO)   
			                                ORDER BY PJ_NAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mProject()
                        {
                            PJ_ID = string.Format("{0:00000}",(int)dr["PJ_ID"]),
                            PJ_NAME = (string)dr["PJ_NAME"]
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }
        #endregion

        #region List ไอเทม
        public List<mItemPj> PCgetProjectItem(iItemPj input)
        {
            List<mItemPj> results = new List<mItemPj>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.PD_ITEM AS ITEM
                                               FROM 
                                                (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                                 UNION
                                                 SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                 GROUP BY P.PJ_ID,P.ITEM) P 
                                             WHERE NOT EXISTS
                	                            (SELECT *
                			                            FROM NCR_CHECK NC    
                                                   WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
                	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                             AND P.PJ_ID = @PJ_ID
                                             ORDER BY PD_ITEM";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mItemPj()
                        {
                            PD_ITEM = dr["ITEM"].ToString()
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }
        #endregion

        #region List Rev No
        public List<mRevNoPlanCheck> PCgetProjectRevno(iRevNoPlanCheck input)
        {
            List<mRevNoPlanCheck> results = new List<mRevNoPlanCheck>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.REV_NO,
                                               CASE WHEN P.REV_NO = 0 THEN 'FINAL' 
                                               ELSE CAST(P.REV_NO AS VARCHAR) END AS REV_NAME
                                                 FROM 
                                                  (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P 
                                                   UNION 
                                                   SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                   GROUP BY P.PJ_ID,P.ITEM) P 
                                               WHERE NOT EXISTS
              	                                (SELECT * 
              			                        FROM NCR_CHECK NC    
                                                WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS < 0
              	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                                AND P.PJ_ID = @PJ_ID
                                                AND P.PD_ITEM = @PD_ITEM
                                                ORDER BY REV_NO";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    command1.Parameters.Add("@PD_ITEM", SqlDbType.Int, 4).Value = input.nPd_Item;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mRevNoPlanCheck()
                        {
                            REV_NO = (int)dr["REV_NO"],
                            REV_NAME = dr["REV_NAME"].ToString()
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }
        #endregion

        #region List Group Fab & Weld
        public List<mGroupFab> PCgetGroupFab()
        {
            List<mGroupFab> results = new List<mGroupFab>();

            McsQcDataContext db = new McsQcDataContext();

            var queryFabGroup = (from f in db.V301_MainProducts group f by f.F_GRP into Fabgroup orderby Fabgroup.Key select Fabgroup.Key);

            foreach (var items in queryFabGroup)
            {
                results.Add(new mGroupFab()
                {
                    F_GRP = items
                });
            }       
            return results;
        }

        public List<mGroupWeld> PCgetGroupWeld()
        {
            List<mGroupWeld> results = new List<mGroupWeld>();

            McsQcDataContext db = new McsQcDataContext();

            var queryWeldGroup = (from w in db.V301_MainProducts group w by w.W_GRP into Weldgroup orderby Weldgroup.Key select Weldgroup.Key);
            foreach (var items in queryWeldGroup)
            {
                results.Add(new mGroupWeld()
                {
                    W_GRP = items
                });
            }
            return results;
        }
        #endregion

        #region List สถานะ
        public List<mStatus> PCgetStatus()
        {
            List<mStatus> results = new List<mStatus>();

            string[] name = new string[] { "0: QC Check", "1: QC Confirm to Repair", "2: PD Repair", "3: QC Recheck", "4: Not Pass", "5: OK" };
            int i = 0;

            foreach (string value in name)
            {
                results.Add(new mStatus()
                {
                    STATUS_ID = (int)i++,
                    STATUS_NAME = (string)value
                });
            }
            return results;
        }
        #endregion

        #region แสดงข้อมูล ncr plan check
        public List<mPjShowListCheck> PCgetProjectShowListCheck(iPjShowListCheck input)
        {
            List<mPjShowListCheck> results = new List<mPjShowListCheck>();

            try
            {
                object[] prm = { input.nTypePc, input.nPj_Id, input.nPd_Item, input.sRev_No, input.sCode, input.sGroupFab, input.sGroupWeld, input.sFab_Date, input.sWeld_Date, input.sNc_Status };
                
                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_PLAN_NCR_CHECK", prm);

                while (reader.Read())
                {
                    results.Add(new mPjShowListCheck()
                    {
                        PJ_NAME = reader["PJ_NAME"].ToString(),
                        PD_ITEM = string.Format("{0:0000}", (Int16)reader["PD_ITEM"]),
                        FW_REV = reader["FW_REV"].ToString(),
                        PD_CODE = reader["PD_CODE"].ToString(),
                        PART = reader["PART"].ToString(),
                        F_GRP = reader["F_GRP"].ToString(),
                        F_DATE = reader["F_DATE"].ToString(),
                        W_GRP = reader["W_GRP"].ToString(),
                        W_DATE = reader["W_DATE"].ToString(),
                        OA_GRP = reader["OA_GRP"].ToString(),
                        OA_DATE = reader["OA_DATE"].ToString(),
                        CHECK_NO = reader["CHECK_NO"].ToString(),
                        COUNT_NCR = reader["COUNT_NCR"].ToString(),
                        DESIGN_CHANGE = reader["DESIGN_CHANGE"].ToString(),
                        NCR_STATUS = reader["NCR_STATUS"].ToString(),
                        USERS = reader["USERS"].ToString(),
                        NCR_UPDATE = reader["NCR_UPDATE"].ToString(),
                        PLACE = reader["PLACE"].ToString()
                    });
                }
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }
        #endregion


        // IN Form NCR------------------------------------------------------------------------------------------------
        #region ListData InFormNcr
        public mListDataForm IFgetListDataForm(iListDataForm input)
        {
            McsQcDataContext db = new McsQcDataContext();

            mListDataForm results = new mListDataForm();
            results.getDwgType = new List<mProjectImage>();
            results.getDirection = new List<mDirection>();

            #region List DWG Type
            if (input.nPt_MainType > 0)
            {
                var queryPjImageT = (from p in db.NCR_FROM_IMGs orderby p.POSITION_ID descending where 1 == 1 && p.PT_GROUP_TYPE_ID == input.nPt_MainType || p.PT_GROUP_TYPE_ID == 0 select p);
                foreach (var items_position in queryPjImageT)
                {
                    results.getDwgType.Add(new mProjectImage()
                    {
                        IMAGE_NAME = items_position.IMG_NAME.ToString(),
                        POSITION_ID = items_position.POSITION_ID.ToString()
                    });
                }
            }
            else
            {
                var queryPjImage = (from p in db.NCR_FROM_IMGs where 1 == 1 select p);
                foreach (var items_position in queryPjImage)
                {
                    results.getDwgType.Add(new mProjectImage()
                    {
                        IMAGE_NAME = items_position.IMG_NAME.ToString(),
                        POSITION_ID = items_position.POSITION_ID.ToString()
                    });
                }
            }
            #endregion

            #region List Direction
            foreach (vt_direction items_dt in db.vt_directions.OrderBy(d => d.drId))
            {
                results.getDirection.Add(new mDirection()
                {
                    DRNAME = items_dt.drName
                });
            }
            #endregion

            return results;
        }
        #endregion

        #region List Rev no
        public List<mRevNoInForm> IFgetProjectRevNo(iRevNoInForm input)
        {
            List<mRevNoInForm> results = new List<mRevNoInForm>();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }


            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.REV_NO,
                                                       CASE WHEN P.REV_NO = 0 THEN 'FINAL' 
                                                       ELSE CAST(P.REV_NO AS VARCHAR) END AS REV_NAME
                                                         FROM 
                                                          (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P 
                                                           UNION 
                                                           SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                           GROUP BY P.PJ_ID,P.ITEM) P 
                                                       WHERE NOT EXISTS
                      	                                (SELECT * 
                      			                        FROM NCR_CHECK NC    
                                                        WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS > 1  AND CHECK_NO = @CHECK_NO
                      	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                                        AND P.PJ_ID = @PJ_ID
                                                        AND P.PD_ITEM = @PD_ITEM
                                                        ORDER BY REV_NO";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@CHECK_NO", SqlDbType.Int, 2).Value = input.nCheck_no;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = project;
                    command1.Parameters.Add("@PD_ITEM", SqlDbType.Int, 4).Value = items;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mRevNoInForm()
                        {
                            REV_NO = (string)dr["REV_NO"].ToString(),
                            REV_NAME = dr["REV_NAME"].ToString()
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }
        #endregion

        #region get Project Detial
        public mChProjectDetail IFgetProjectDetail(iChProjectDetail input)
        {
            McsAppDataContext db = new McsAppDataContext();
            McsQcDataContext qc = new McsQcDataContext();
            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            mChProjectDetail results = new mChProjectDetail();

            try
            {
                if (project != 0)
                {
                    Project items_pj = db.Projects.Where(pj => pj.pj_id == project).FirstOrDefault();

                    if (items_pj != null)
                    {
                        results.PJ_NAME = items_pj.pj_name.ToString() + "-s" + items_pj.pj_sharp + "-Lot" + items_pj.pj_lot;
                    }
                    else
                    {
                        results.PJ_NAME = "====";
                    }

                    NCR_CHECK item_nc = qc.NCR_CHECKs.Where(nc => nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_no && nc.CHECK_NO == 1).FirstOrDefault();

                    if (item_nc != null)
                    {
                        results.NC_NCR = (int)item_nc.NC_NCR;
                        results.PLACE = (string)item_nc.PLAN_INS;
                    }
                    else
                    {
                        results.NC_NCR = 0;
                        results.PLACE = "";
                    }

                    if (results.PLACE == "")
                    {
                        Product place = db.Products.Where(p => p.pj_id == project && p.pd_item == items).FirstOrDefault();
                        if (place != null)
                        {
                            results.PLACE = (string)place.place;
                        }
                        else
                        {
                            results.PLACE = "";
                        }
                    }
                }

                object[] prm = { input.nTypePc, project, items, input.nRev_no };
                var reader1 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);

                string pa_state = "";

                if (reader1.HasRows)
                {
                    while (reader1.Read())
                    {
                        pa_state = reader1["PA_STATE"].ToString();
                    }

                    if (pa_state == "1")
                    {
                        var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);
                        while (reader2.Read())
                        {
                            results.ERROR = (string)reader2["ERROR"];
                            results.PLAN_DATE = ":: No Plan Date ::";
                            results.PD_CODE = ":: No Code ::";
                            results.PT_MAINTYPE = "0";
                            results.BARCODE = "";
                        }
                    }
                    else
                    {
                        var reader2 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_CHECK_PRODUCT_DETAIL", prm);
                        while (reader2.Read())
                        {
                            results.PLAN_DATE = Convert.ToDateTime(reader2["PLAN_DATE"]).ToString("yyyy-MM-dd HH:mm");
                            results.PD_CODE = (string)reader2["PD_CODE"];
                            results.PT_MAINTYPE = reader2["PT_MAINTYPE"].ToString();
                            results.BARCODE = (string)reader2["BARCODE"];
                            results.ERROR = "";
                        }
                    }
                }
                else
                {
                    results.PLAN_DATE = "";
                    results.PD_CODE = "";
                    results.PT_MAINTYPE = "";
                    results.BARCODE = "";
                    results.ERROR = "";
                    results.PJ_NAME = "";
                    results.NC_NCR = 0;
                    results.UG_NAME = "";
                    results.PLACE = "";
                }
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }
        #endregion

        #region List ตำแหน่ง
        public List<mPosition> IFgetPositionList(iPosition input)
        {
            List<mPosition> results = new List<mPosition>();

            McsQcDataContext db = new McsQcDataContext();

            var queryNcId = from nc in db.NCR_CHECK_LISTs where nc.PT_GROUP_TYPE_ID == input.nPt_MainType select nc.NT_ID;
            var queryPosition = from cl in db.NCR_CHECK_LIST_TYPEs where cl.PC_ID == input.nTypePc && queryNcId.Contains(cl.NT_ID) select cl;

            foreach (var items in queryPosition)
            {
                results.Add(new mPosition()
                {
                    NT_NAME = items.NT_NAME,
                    NT_ID = items.NT_ID.ToString()
                });
            }
            return results;
        }
        #endregion

        #region List ความผิดพลาด
        public List<mDescriptions> IFgetDescriptionsNcr(iDescriptions input)
        {
            List<mDescriptions> results = new List<mDescriptions>();

            McsQcDataContext db = new McsQcDataContext();

            var queryDescNce = from cl in db.NCR_CHECK_LISTs
                               join nd in db.NCR_DESCs
                               on cl.DESC_ID equals nd.DESC_ID
                               where cl.PT_GROUP_TYPE_ID == input.nPt_MainType && cl.NT_ID == input.nNt_Id
                               orderby nd.DESCRIPTIONS
                               select new { cl.NCR_NO, nd.DESCRIPTIONS };

            foreach (var items in queryDescNce)
            {
                results.Add(new mDescriptions()
                {
                    DESCRIPTIONS = items.DESCRIPTIONS,
                    NCR_NO = items.NCR_NO.ToString()
                });
            }
            return results;
        }
        #endregion

        public mgetOther IFgetOther(igetOther input)
        {
            mgetOther result = new mgetOther();

            McsQcDataContext db = new McsQcDataContext();

            NCR_DIM_CHECK icheck = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_id && nd.NCR_NO == input.sNcr_no).FirstOrDefault();

            if (icheck == null)
            {
                result.OTHER_NO = "1";
            }
            else
            {
                var query = (from nd in db.NCR_DIM_CHECKs
                             where nd.NC_ID == input.sNc_id && nd.NCR_NO == input.sNcr_no
                             select nd.OTHER_NO).Max() + 1;

                result.OTHER_NO = query.ToString();
            }
            return result;
        }


        #region แสดงกรุ๊ปที่รับผิดชอบ
        public mResponse IFgetResponse(iResponse input)
        {
            mResponse results = new mResponse();

            int project = 0;
            int items = 0;
            int point = input.sBarcode.IndexOf("-");

            if (point == -1)
            {
                project = 0;
                items = 0;
            }
            else
            {
                project = Convert.ToInt32(input.sBarcode.Substring(0, point));
                items = Convert.ToInt16(input.sBarcode.Substring(point + 1));
            }

            try
            {
                string group = "";
                string process;

                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK_LIST icheck = db.NCR_CHECK_LISTs.Where(nc => nc.NCR_NO.Equals(input.sNcr_no)).FirstOrDefault();

                if (icheck == null)
                {
                    group = "";
                    results.DESC_CHK = 0;
                }
                else
                {
                        group = icheck.RESPONS.ToString();
                        results.DESC_CHK = (int)icheck.DESC_CHK;
                }

                if (group != "")
                {
                    process = Position_Ncr_Process(input.sNcr_no);
                    if (Show_Group(project, items, process, 0) != "")
                    {
                        group += " / ";
                    }
                    group += Show_Group(project, items, process, 0);
                }
                else if (group == "")
                {
                    process = Position_Ncr_Process(input.sNcr_no);
                    group += Show_Group(project, items, process, 0);
                }
                results.RESPONSE_GROUP = group;
            }
            catch (Exception)
            {
                throw;
            }
            return results;
        }

        private string Position_Ncr_Process(string sNcr_no)
        {
            string process = "";
            string value_pc = "";

            McsQcDataContext db = new McsQcDataContext();

            foreach (NCR_RESPON items in db.NCR_RESPONs.Where(nc => nc.NCR_NO.Equals(sNcr_no)))
            {
                value_pc = items.PC_ID.ToString();
            }

            if (process != "")
            {
                process += "," + value_pc.ToString();
            }
            else
            {
                process = value_pc.ToString();
            }
            return process;
        }

        private string Show_Group(int nPj_Id, int nPd_Item, string sTypePc, int nRev_no)
        {
            string Group = "";
            string Value_g = "";
            //int i;
            string[] Str = sTypePc.Split(',');
            for (int i = 0; i < Str.Length; i++)
            {
                McsAppDataContext db = new McsAppDataContext();

                if (Str[i].Substring(0, 1) == "5")
                {
                    if (nRev_no == 0)
                    {
                        var query = from GFW in db.SF_GET_GROUP_FAB_WELD(nPj_Id, nPd_Item, nRev_no, Str[i]) select new { group_name = GFW.ToString() };

                        foreach (var item_g in query)
                        {
                            Value_g += item_g.group_name.ToString();
                        }

                        if (Group != "")
                        {
                            Group += " / " + Value_g.ToString();
                        }
                        else
                        {
                            Group += Value_g.ToString();
                        }
                    }
                }
                else if (Str[i].Substring(0, 1) == "1")
                {
                    var query = from GB in db.SF_GET_GROUP_BUILT(nPj_Id, nPd_Item, Str[i]) select new { group_name = GB };

                    foreach (var items in query)
                    {
                        Value_g = items.group_name.ToString();
                    }

                    if (Group != "")
                    {
                        Group += " / " + Value_g.ToString();
                    }
                    else
                    {
                        Group += Value_g.ToString();
                    }
                }
            }
            return Group;
        }
        #endregion   

        #region แจ้ง NCR  : แบบมี NCR
        public mResult IFsetAddNcrCheck(iNcrCheckAdd input)
        {
            mResult result = new mResult();
          
            try
            {
                McsQcDataContext db = new McsQcDataContext();
                McsFg2010DataContext fg = new McsFg2010DataContext();

                    var icheck = (from nd in db.NCR_DIM_CHECKs
                                  join nc in db.NCR_CHECKs on nd.NC_ID equals nc.NC_ID
                                  where nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_no && nc.CHECK_NO == input.nCheck_no && nd.NCR_NO == input.sNcr_no && nd.OTHER_NO == input.nOther_no
                                  select nd).FirstOrDefault();
                    //เช็ค NCR นี้เคยส่งหรือยัง
                    if (icheck == null)
                    {
                        //เช็คพื้นที่
                        var check_st = (from ss in fg.stock_shelfs where ss.place == input.sPlanIns select ss.place).FirstOrDefault();
                        if (check_st != null)
                        {
                            //เช็ครูปภาพ
                            if (input.Get_Image == true)
                            {
                                string sImage_Name = input.sNc_id + input.sNcr_no + "-" + input.nOther_no + ".jpg";

                                object[] prm = { input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, input.sNcr_no, input.sUser, input.sUser, input.sGroup, sImage_Name, input.sSug, input.sDirection, input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1 };

                                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                                while (reader.Read())
                                {
                                    result.RESULT = reader["ERROR"].ToString();
                                    result.EXCEPTION = "";
                                }
                            }
                            else
                            {
                                object[] prm = { input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, input.sNcr_no, input.sUser, input.sUser, input.sGroup, "No.jpg", input.sSug, input.sDirection, input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1 };

                                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                                while (reader.Read())
                                {
                                    result.RESULT = reader["ERROR"].ToString();
                                    result.EXCEPTION = "";
                                }
                            }
                        }
                        else
                        {
                            result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                            result.EXCEPTION = "";
                        }
                    }
                    else
                    {
                        result.RESULT = "ความผิดพลาด(NCR)นี้แจ้งไปแล้ว";
                        result.EXCEPTION = "";
                    }
                }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region แก้ไข NCR
        public mResult IFsetEditNcrCheck(iNcrCheckEdit input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();
                McsFg2010DataContext fg = new McsFg2010DataContext();

                var check_st = (from ss in fg.stock_shelfs where ss.place == input.sPlace select ss.place).FirstOrDefault();
                if (check_st != null)
                {

                    var query_nc = (from nc in db.NCR_CHECKs
                                    where nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode &&
                                        nc.REV_NO == input.nRev_No && nc.CHECK_NO == input.nCheck_No
                                    select nc).FirstOrDefault();

                    var query_nd = (from nd in db.NCR_DIM_CHECKs
                                    where nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no &&
                                    (from nc in db.NCR_CHECKs
                                     where nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_No && nc.CHECK_NO == input.nCheck_No
                                     select new { nc.NC_ID }).Contains(new { NC_ID = nd.NC_ID })
                                    select nd).FirstOrDefault();


                    if (input.Get_Image == true)
                    {
                        query_nd.IMG_BEFORE = input.sFile;
                        query_nd.USER_UPDATE = input.sUsers;
                        query_nd.TIME_UPDATE = (DateTime)(now);
                        query_nd.SUGGEST = input.sSug;
                        query_nd.DIRECTION = input.sDirection;
                        query_nd.GROUP_NAME = input.sGroup;
                        query_nd.DWG = input.Dwg;
                        query_nd.ACTUAL = input.Actual;
                        query_nc.PLAN_INS = input.sPlace;
                    }
                    else
                    {
                        query_nd.IMG_BEFORE = input.sFile;
                        query_nd.USER_UPDATE = input.sUsers;
                        query_nd.TIME_UPDATE = (DateTime)(now);
                        query_nd.SUGGEST = input.sSug;
                        query_nd.DIRECTION = input.sDirection;
                        query_nd.GROUP_NAME = input.sGroup;
                        query_nd.DWG = input.Dwg;
                        query_nd.ACTUAL = input.Actual;
                        query_nc.PLAN_INS = input.sPlace;
                    }

                    db.SubmitChanges();

                    result.RESULT = "1|แก้ไขข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";
                }
                else
                {
                    result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถทำการแก้ไขข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region ลบ NCR
        public mResult IFsetDeleteNcrCheck(iNcrCheckDelete input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                if (input.sImage_Before != "No.jpg")
                {
                    string img = DeleteImage(input.sImage_Before);
                }

                var Del_Ncr = from nd in db.NCR_DIM_CHECKs
                              where nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no
                              select nd;

                foreach (var items in Del_Ncr)
                {
                    db.NCR_DIM_CHECKs.DeleteOnSubmit(items);
                }

                db.SubmitChanges();

                NCR_DIM_CHECK icheck = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_Id).FirstOrDefault();

                if (icheck == null)
                {
                    NCR_CHECK ncr_check = db.NCR_CHECKs.Where(nc => nc.NC_ID == input.sNc_Id).FirstOrDefault();

                    ncr_check.NC_NCR = 0;
                    ncr_check.NC_STATUS = 1;
                    ncr_check.USER_UPDATE = input.sUser;
                    ncr_check.TIME_UPDATE = (DateTime)(now);
                }

                db.SubmitChanges();

                result.RESULT = "ลบรายการแจ้งความผิดพลาด(NCR)เรียบร้อยแล้วค่ะ.";
                result.EXCEPTION = "";

                return result;

            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถลบรายการแจ้งความผิดพลาด(NCR)ได้ค่ะ.";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region อัพโหลดรูปภาพ
        public mResult uploadImages_QC(Stream fileUpload)
        {
            mResult result = new mResult();

            try
            {
                McsAppDataContext dc = new McsAppDataContext();

                string servPath = "";
                string fileSize = "";

                int point = 0;
                int width = 0;
                int height = 0;

                foreach (PDPC_config items in dc.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_BEFORE")) servPath = items.config_value;
                    if (items.config_key.Equals("PICTURE")) fileSize = items.config_value;
                }

                if (fileSize != "")
                {
                    point = fileSize.IndexOf("x");
                    width = Convert.ToInt32(fileSize.Substring(0, point));
                    height = Convert.ToInt32(fileSize.Substring(point + 1));
                }

                //------------------------------------------------------------------------------------------------------

                var parser = new MultipartFormDataParser(fileUpload);

                var username = parser.Parameters["username"].Data;
                var barcode = parser.Parameters["barcode"].Data;
                var otherno = parser.Parameters["otherno"].Data;
                var imagename = parser.Parameters["imagename"].Data;

                var file = parser.Files.FirstOrDefault();

                Stream stream = file.Data;

                //------------------------------------------------------------------------------------------------------

                if (!Directory.Exists(servPath)) Directory.CreateDirectory(servPath);

                FileStream targetStream = null;
                Stream sourceStream = stream;

                string fileName = imagename + "-" + otherno + "-X.jpeg";
                string filePath = Path.Combine(servPath, fileName);

                using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    const int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    int totalBytes = 0;

                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        totalBytes += count;

                        targetStream.Write(buffer, 0, count);
                    }

                    targetStream.Close();

                    sourceStream.Close();
                }

                resizeImage(filePath, width, height);

                result.RESULT = "อัพโหลดรูปภาพเรียบร้อยแล้ว";
                result.EXCEPTION = "";
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถอัพโหลดรูปภาพได้";
                result.EXCEPTION = ex.Message;
            }

            return result;
        }
        #endregion

        #region Delete Image
        private string DeleteImage(string img_name)
        {
            string result = "";
            string servPath = "";
            try
            {
                McsAppDataContext db = new McsAppDataContext();

                foreach (PDPC_config items in db.PDPC_configs)
                {
                    if (items.config_key.Equals("PATH_BEFORE")) servPath = items.config_value;
                }

                string filePath = Path.Combine(servPath, img_name);

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                    result = "ลบรูปภาพแล้ว";
                }
                else
                {
                    result = "ไม่มีรูปภาพ";
                }             
            }
            catch (Exception ex)
            {               
                result = ex.Message;
            }

            return result;
        }
        #endregion

        #region ส่ง Ncr : แบบไม่มี NCR
        public mResult IFsetNoNcrCheck(iNoNcrCheck input)
        {
            mResult result = new mResult();
            McsFg2010DataContext fg = new McsFg2010DataContext();

            try
            {
                var check_st = (from ss in fg.stock_shelfs where ss.place == input.sPlanIns select ss.place).FirstOrDefault();
                if (check_st != null)
                {
                    if (input.sBarcode != "" && input.sPlanDate != "")
                    {
                        object[] prm = { input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no, "NULL", input.sUser, input.sUser, "NULL", "NULL", "NULL", "NULL", input.nDwg_type, input.sPlanDate, input.sPlanIns, null, null, 5 };

                        var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                        while (reader.Read())
                        {
                            result.RESULT = reader["ERROR"].ToString();
                            result.EXCEPTION = "";
                        }
                    }
                    else
                    {
                        result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                        result.EXCEPTION = "";
                    }
                }
                else
                {
                    result.RESULT = "กรุณาป้อนข้อมูลพื้นที่ให้ถูกต้อง";
                    result.EXCEPTION = "";
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region แสดงรายการ NCR
        public List<mNcrReport> IFgetNcrReport(iNcrReport input)
        {
            string other_no = "";
            List<mNcrReport> results = new List<mNcrReport>();

            object[] prm = { input.sNc_Id, input.sNc_State,"","" };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    other_no = reader["OTHER_NO"].ToString();
                }

                if (other_no != "")
                {
                    var reader1 = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

                    while (reader1.Read())
                    {
                        results.Add(new mNcrReport
                        {
                            NCR_NO = reader1["NCR_NO"].ToString(),
                            DIRECTION = reader1["DIRECTION"].ToString(),
                            POSITION = reader1["NT_NAME"].ToString(),
                            DESCRIPTIONS = reader1["DESCRIPTIONS"].ToString(),
                            IMG_BEFORE = reader1["IMG_BEFORE"].ToString(),
                            RESPONSE = reader1["GROUP_NAME"].ToString(),
                            DWG = reader1["DWG"].ToString(),
                            ACTUAL = reader1["ACTUAL"].ToString(),
                            OTHER_NO = (int)reader1["OTHER_NO"],
                            SUGGEST = reader1["SUGGEST"].ToString(),
                            PLACE = reader1["PLACE"].ToString()
                        });
                    }
                }
                else
                {
                    results.Add(new mNcrReport
                    {
                        NCR_NO = "-",
                        DIRECTION = "-",
                        POSITION = "-",
                        DESCRIPTIONS = "-",
                        IMG_BEFORE = "-",
                        RESPONSE = "-",
                        DWG = "-",
                        ACTUAL = "-",
                        OTHER_NO = 0,
                        SUGGEST = "-",
                        PLACE = "",
                    });
                }
            }
            else
            {
                results.Add(new mNcrReport
                {
                    NCR_NO = "-",
                    DIRECTION = "-",
                    POSITION = "-",
                    DESCRIPTIONS = "-",
                    IMG_BEFORE = "-",
                    RESPONSE = "-",
                    DWG = "-",
                    ACTUAL = "-",
                    OTHER_NO = 0,
                    SUGGEST = "-",
                    PLACE = ""
                });
            }
            return results;
        }
        #endregion

        #region แสดง Ncr Type
        public mNcrType IFgetNcrType(iNcrType input)
        {
            mNcrType results = new mNcrType();

            McsQcDataContext db = new McsQcDataContext();

            NCR_CHECK_LIST icheck = db.NCR_CHECK_LISTs.Where(nl => nl.NCR_NO == input.sNcr_No).FirstOrDefault();

            if (icheck == null)
            {
                results.NT_ID = "====";
            }
            else
            {
                results.NT_ID = icheck.NT_ID.ToString();
            }
            return results;
        }
        #endregion

        #region ยืนยันการแจ้ง NCR
        public mResult IFsetConfirmNcr(iConfirmNcr input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK items = db.NCR_CHECKs.Where(nc => nc.PC_ID == input.nTypePc && nc.BARCODE == input.sBarcode && nc.REV_NO == input.nRev_No && nc.CHECK_NO == input.nCheck_No).FirstOrDefault();

                if (items != null)
                {
                    items.NC_STATUS = 1;
                    items.DATE_FINISH = (DateTime)(now);
                    items.USER_UPDATE = input.sUser;
                    items.TIME_UPDATE = (DateTime)(now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันการแจ้งความผิดพลาด(NCR)เรียบร้อยแล้วค่ะ.";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch(Exception ex)
            {
                result.RESULT = "2|ยืนยันการแจ้งความผิดพลาด(NCR)ไม่สำเร็จค่ะ.";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion


        // Qc Recheck ----------------------------------------------------------------------------------------------
        #region แสดงข้อมูล NCR Recheck
        public mDataRecheck RCgetDataRecheck(iDataRecheck input)
        {
            mDataRecheck result = new mDataRecheck();

           // '9100001058-0001001"
        int nTypePc = Convert.ToInt32(input.sNc_Id.Substring(0, 5));
        string sBarcode = input.sNc_Id.Substring(5, 10);
        int nRev = Convert.ToInt32( input.sNc_Id.Substring(15, 2));
        int nCheckNo = Convert.ToInt32(input.sNc_Id.Substring(17, 1));

            object[] prm = { input.sNc_Id };

            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR_EDIT", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.PJ_NAME = reader["PJ_NAME"].ToString();
                    result.PD_ITEM = (Int16)reader["PD_ITEM"];
                    result.REV_NO = reader["REV_NO"].ToString();
                    result.CHECK_NO = nCheckNo;
                    result.PD_CODE = reader["PD_CODE"].ToString();
                    result.DATE_PLAN = reader["DATE_PLAN"].ToString();
                    result.USERNAME = reader["USERNAME"].ToString();
                    result.DATE_START = reader["DATE_START"].ToString();
                    result.BARCODE = reader["BARCODE"].ToString();
                    result.PT_MAINTYPE =(int)reader["PT_MAINTYPE"];
                }
            }
            else
            {
                result.PJ_NAME  = "-";
                result.PD_ITEM = 0;
                result.REV_NO = "-";
                result.CHECK_NO   =0; 
                result.PD_CODE  = "-";
                result.DATE_PLAN  = "-";
                result.USERNAME  = "-";
                result.DATE_START  = "-";
                result.BARCODE  = "-";
                result.PT_MAINTYPE = 0;
            }
            return result;
        }
        #endregion

        #region แสดง Report NCR Recheck
        public List<mNcrReportRC> RCgetNcrReport(iNcrReportRC input)
        {
            List<mNcrReportRC> results = new List<mNcrReportRC>();
            string pc_id = input.sNc_Id.Substring(0, 5);
            string barcode = input.sNc_Id.Substring(5, 10);
            string rev_no = input.sNc_Id.Substring(15, 2);

            object[] prm = { input.sNc_Id, "3", "3" ,"", pc_id, barcode, rev_no};
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.Add(new mNcrReportRC()
                    {
                        NCR_NO = reader["NCR_NO"].ToString(), 
                        NT_NAME = reader["NT_NAME"].ToString(),
                        DIRECTION = reader["DIRECTION"].ToString(),
                        DESCRIPTIONS = reader["DESCRIPTIONS"].ToString(),
                        GROUP_NAME = reader["GROUP_NAME"].ToString(),
                        FULLNAME_PD = reader["FULLNAME_PD"].ToString(),
                        DATE_PD = reader["DATE_PD"].ToString(),
                        DWG = reader["DWG"].ToString(),
                        ACTUAL = reader["ACTUAL"].ToString(),
                        SUGGEST = reader["SUGGEST"].ToString(),
                        OTHER_NO = reader["OTHER_NO"].ToString(),
                        POSITION_ID = reader["POSITION_ID"].ToString()
                    });
                }
            }
            else
            {
                results.Add(new mNcrReportRC()
                {
                    NCR_NO = "-",
                    NT_NAME = "",
                    DIRECTION = "",
                    DESCRIPTIONS = "",
                    GROUP_NAME = "",
                    FULLNAME_PD = "",
                    DATE_PD = "",
                    DWG = "",
                    ACTUAL = "",
                    SUGGEST = "",
                    OTHER_NO ="",
                    POSITION_ID = ""
                });
            }
            return results;
        }
        #endregion

        #region Update Recheck OK
        public mResult RCsetRecheckOK(iRecheckOK input)
        {
            mResult result = new mResult();
            
            DateTime now = DateTime.Now;
            
            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_DIM_CHECK items = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_no).FirstOrDefault();

                if (items != null)
                {
                    items.ND_STATE = 5;
                    items.IMG_AFTER = (input.sFile == "") ? "No.jpg" : input.sFile;
                    items.QC_RECHECK = input.sUsers;
                    items.DATE_RECHECK = (DateTime)(now);
                    items.USER_UPDATE = input.sUsers;
                    items.TIME_UPDATE = (DateTime)(now);
                    items.ACTUAL = input.Actual;

                    db.SubmitChanges();

                    result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #region Update Recheck Not OK
        public mResult RCsetRecheckNotOK(iRecheckNotOK input)
        {
            mResult result = new mResult();

            DateTime now = DateTime.Now;

            string error = "";

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                object[] prm = { input.nTypePc, input.sBarcode, input.nRev_no, input.nCheck_no + 1, input.sNcr_no, input.sUser, input.sUser, input.sGroup, (input.sFile == "") ? "No.jpg" : input.sFile, input.sSug, input.sDirection, input.nDwg_type, input.sPlanDate, input.sPlanIns, input.Dwg, input.Actual, 1 };
                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_SET_NCR_CHECK", prm);

                while (reader.Read())
                {
                    error = reader["ERROR"].ToString();                  
                }

                if (error.Substring(0, 1) == "1")
                {
                    NCR_DIM_CHECK items = db.NCR_DIM_CHECKs.Where(nd => nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_no && nd.OTHER_NO == input.nOther_No).FirstOrDefault();

                    if (items != null)
                    {
                        items.ND_STATE = 4;
                        items.QC_RECHECK = input.sUser;
                        items.DATE_RECHECK = (DateTime)(now);
                        items.USER_UPDATE = input.sUser;
                        items.TIME_UPDATE = (DateTime)(now);

                        db.SubmitChanges();

                        result.RESULT = "1|บันทึกข้อมูลเรียบร้อยแล้ว";
                        result.EXCEPTION = "";
                    }                  
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;  
        }
        #endregion

        #region Delete Ncr Recheck
        public mResult RCsetRecheckDel(iRecheckDel input)
        {
            mResult result = new mResult();

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                if (input.sImg_Before == "None.jpg" || input.sImg_Before == null)
                {
                    result.RESULT = "ยังไม่ได้ทำการอัพโหลดรูปภาพ";
                    result.EXCEPTION = "";
                }
                else
                {
                    string img = DeleteImage(input.sImg_Before);

                    if (img == "T")
                    {

                        var Del_Ncr = from nd in db.NCR_DIM_CHECKs
                                      where nd.NC_ID == input.sNc_Id && nd.NCR_NO == input.sNcr_No && nd.OTHER_NO == input.nOther_No
                                      select nd;

                        foreach (var items in Del_Ncr)
                        {
                            db.NCR_DIM_CHECKs.DeleteOnSubmit(items);
                        }

                        db.SubmitChanges();

                        result.RESULT = "ลบภาพความผิดพลาด(NCR)เรียบร้อยแล้ว";
                        result.EXCEPTION = "";

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถลบข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion


        //Qc Finish Check 
        #region List ชื่อโปรเจค
        public List<mProject> FNgetProjectName(iProject input)
        {
            List<mProject> results = new List<mProject>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT  MCSAPP.DBO.SF_FULLNAME(P.PJ_ID) PJ_NAME,P.PJ_ID 
                                            FROM 
                                            (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                            UNION 
                                            SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                            GROUP BY P.PJ_ID,P.ITEM) P 
				                            WHERE NOT EXISTS
              			                    (SELECT *
              			                    FROM NCR_CHECK NC    
						                    WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS = 1
              			                    AND P.PJ_ID = CAST(LEFT(NC.BARCODE,5) AS INT)  AND P.PD_ITEM = CAST(RIGHT(NC.BARCODE,4) AS INT)   AND P.REV_NO = NC.REV_NO)   
			                                ORDER BY PJ_NAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mProject()
                        {
                            PJ_ID = string.Format("{0:00000}", (int)dr["PJ_ID"]),
                            PJ_NAME = (string)dr["PJ_NAME"]
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }
        #endregion

        #region List ไอเทม
        public List<mItemPj> FNgetProjectItem(iItemPj input)
        {
            List<mItemPj> results = new List<mItemPj>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT P.PD_ITEM AS ITEM
                                               FROM 
                                                (SELECT P.PJ_ID, P.PD_ITEM ,0 AS REV_NO FROM MCSAPP..PRODUCT P  
                                                 UNION
                                                 SELECT P.PJ_ID,P.ITEM  ,MAX(REV_NO) REV_NO FROM MCSAPP..REV_PLAN P 
                                                 GROUP BY P.PJ_ID,P.ITEM) P 
                                             WHERE EXISTS
                	                            (SELECT *
                			                            FROM NCR_CHECK NC    
                                                   WHERE NC.PC_ID = @TYPE_PC And NC.NC_STATUS = 1
                	                                AND MCSAPP.DBO.SF_BARCODE(P.PJ_ID,P.PD_ITEM) = NC.BARCODE AND P.REV_NO = NC.REV_NO) 
                                             AND P.PJ_ID = @PJ_ID
                                             ORDER BY PD_ITEM";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    command1.Parameters.Add("@PJ_ID", SqlDbType.Int, 5).Value = input.nPj_Id;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mItemPj()
                        {
                            PD_ITEM = dr["ITEM"].ToString()
                        });
                    }
                }

                connection.Close();
            }

            return results;
        }
        #endregion

        #region List Checker
        public List<mChecker> FNgetChecker(iChecker input)
        {
            List<mChecker> results = new List<mChecker>();

            using (SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["McsAppConnectionString"].ConnectionString))
            {
                connection.Open();

                using (SqlCommand command1 = new SqlCommand())
                {
                    command1.Connection = connection;
                    command1.CommandText = @"SELECT DISTINCT USERS,MCSAPP.DBO.SF_GETNAMES(NC.USERS) USERNAME 
                                             FROM NCR_CHECK NC 
                                             WHERE NC.PC_ID = @TYPE_PC
                                             AND NC.NC_STATUS IN (1) AND NC.NC_NCR IN (1)
                                             AND LEN(USERS) = 7
                                             ORDER BY USERNAME";
                    command1.CommandType = CommandType.Text;
                    command1.Parameters.Add("@TYPE_PC", SqlDbType.Int, 5).Value = input.nTypePc;
                    SqlDataReader dr = command1.ExecuteReader();

                    while (dr.Read())
                    {
                        results.Add(new mChecker()
                        {
                            USERS = (string)dr["USERS"],
                            FULLNAME = (string)dr["USERNAME"]
                        });
                    }
                }

                connection.Close();
            }
            return results;
        }
        #endregion

        #region แสดงรายการ Ncr Report List
        public List<mNcrReportFN> FNgetNcrReport(iNcrReportFN input)
        {
            List<mNcrReportFN> results = new List<mNcrReportFN>();

                object[] prm = { input.nTypePc, 1, input.nPj_Id, input.nPd_Item, input.sUser_Check, "0", input.sDate_Edit, input.sPd_Code, "", "", "" };

                var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsQc, "SP_GET_REPORT_NCR_LIST", prm);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        results.Add(new mNcrReportFN()
                        {
                            PJ_NAME = reader["PJ_NAME"].ToString(),
                            PD_ITEM = reader["PD_ITEM"].ToString(),
                            REV_NO = reader["REV_NO"].ToString(),
                            CHECK_NO = (int)reader["CHECK_NO"],
                            PD_CODE = reader["PD_CODE"].ToString(),
                            USERNAME = reader["USERNAME"].ToString(),
                            DATE_START = reader["DATE_START"].ToString(),
                            COUNT_NCR = reader["COUNT_NCR"].ToString(),
                            NC_NCR = Check((int)reader["NC_NCR"]),
                            BARCODE = reader["BARCODE"].ToString(),
                            NC_ID = reader["NC_ID"].ToString(),
                            PLACE = reader["PLACE"].ToString()
                        });
                    }
                }
                else
                {
                    results.Add(new mNcrReportFN()
                    {
                        PJ_NAME = "-",
                        PD_ITEM = "-",
                        REV_NO = "-",
                        CHECK_NO = 0,
                        PD_CODE = "-",
                        USERNAME = "-",
                        DATE_START = "-",
                        COUNT_NCR = "-",
                        NC_NCR = "-",
                        BARCODE = "-",
                        NC_ID = "-",
                        PLACE = ""
                    });
                }         
            return results;
        }

        private string Check(int id)
        {
            string result ="";


            if (id == 0)
            {
                result = "ผ่าน";
            }
            else if (id == 1)
            {
                result = "ไม่ผ่าน";
            }
            else
            {
                result = "ไม่มีผล";
            }
            return result;
        }
        #endregion

        #region Update Qc finish Check
        public mResult FNsetNcrQcCheck(iNcrCheckFN input)
        {
            mResult result = new mResult();
            DateTime now = DateTime.Now;

            try
            {
                McsQcDataContext db = new McsQcDataContext();

                NCR_CHECK icheck = db.NCR_CHECKs.Where(nc => nc.NC_ID == input.sNc_Id).FirstOrDefault();

                if (icheck.NC_NCR >= 1)
                {
                    icheck.NC_STATUS = 2;
                    icheck.USER_QC = input.sUsers;
                    icheck.DATE_QC = (DateTime)(now);
                    icheck.USER_UPDATE = input.sUsers;
                    icheck.TIME_UPDATE = (DateTime)(now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
                else if (icheck.NC_NCR == 0)
                {
                    icheck.NC_STATUS = 5;
                    icheck.USER_QC = input.sUsers;
                    icheck.DATE_QC = (DateTime)(now);
                    icheck.USER_UPDATE = input.sUsers;
                    icheck.TIME_UPDATE = (DateTime)(now);

                    db.SubmitChanges();

                    result.RESULT = "1|ยืนยันข้อมูลเรียบร้อยแล้ว";
                    result.EXCEPTION = "";

                    return result;
                }
            }
            catch (Exception ex)
            {
                result.RESULT = "ไม่สามารถบันทึกข้อมูลได้";
                result.EXCEPTION = ex.Message;
            }
            return result;
        }
        #endregion

        #endregion

        #region HR
        public mListYearAndUser getUserInGroup(iUserInGroup input)
        {
            mListYearAndUser results = new mListYearAndUser();
            results.getYear = new List<mYear>();
            results.getUserIngroup = new List<mUserInGroup>();

            int[] year = new int[2];
            int thisYear =  Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            int pastYear = thisYear -1;
            year[0] = thisYear;
            year[1] = pastYear;

            foreach (int value in year)
            {
                results.getYear.Add(new mYear
                {
                    YEAR = (int)value
                });
            }

            object[] prm = { input.sUs_Id };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsApp, "SP_GET_USER_IN_GROUP_BY_USER", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.getUserIngroup.Add(new mUserInGroup()
                    {
                        US_ID = reader["US_ID"].ToString(),
                        FULLNAME = reader["FULLNAME"].ToString()
                    });
                }
            }
            else
            {
                results.getUserIngroup.Add(new mUserInGroup()
                {
                    US_ID = "====",
                    FULLNAME = "===="
                });
            }
            return results;
        }

        public List<mLeaveQuotaByYear> getMasterLeaveQuotaByYear(iLeaveQuotaByYear input)
        {
            List<mLeaveQuotaByYear> results = new List<mLeaveQuotaByYear>();
            
            object[] prm = { input.sUs_Id, input.nYear };
            var reader = DataAccess.SqlHelper.ExecuteReader(ConnMcsHrMs, "GET_MASTER_LEAVE_QUOTA_BY_YEAR", prm);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    results.Add(new mLeaveQuotaByYear
                    {
                        LEAVE_TYPE_MSG = reader["LEAVE_TYPE_MSG"].ToString(),
                        NUMBER_OF_QUOTA = reader["NUMBER_OF_QUOTA"].ToString(),
                        NUMBER_OF_QUOTA_USAGE = reader["NUMBER_OF_QUOTA_USAGE"].ToString(),
                        LEAVE_REMAIN = (reader["NUMBER_OF_QUOTA"].ToString() == "-") ? 0 : float.Parse(reader["NUMBER_OF_QUOTA"].ToString()) - float.Parse(reader["NUMBER_OF_QUOTA_USAGE"].ToString()),
                        LIFETIMES = (reader["NUMBER_OF_QUOTA"].ToString() == "-") ? "" : reader["LIFETIMES"].ToString()
                    });
                }
            }
            else
            {
                results.Add(new mLeaveQuotaByYear()
                {
                    LEAVE_TYPE_MSG = "-",
                    NUMBER_OF_QUOTA = "-",
                    NUMBER_OF_QUOTA_USAGE = "-",
                    LEAVE_REMAIN = 0,
                    LIFETIMES = "-"
                });
            }

            return results;
        }

        #endregion

    }
}